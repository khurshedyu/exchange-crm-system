$(function() {
	$('#item_details').summernote({
		minHeight: 100,
		maxHeight: 500,
		height: 200,
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'hr']],
			['view', ['fullscreen', 'codeview']]
		],
	});

	$('textarea[name="item_details_content"]').html($('#item_details').code());

	$(".upload-btn").click(function () {
		$("#pledge-images").trigger('click');
	});

	$('#pledge-images').MultiFile({
		accept: 'gif|jpg|png',
		max:10,
		STRING: {
			remove: 'x',
			denied: 'Вы не можете использовать $ext формат.\nИспользуйте следующие форматы: "*jpg, *png, *gif".',
			file: '$file',
			selected: 'File selected: $file',
			duplicate: 'This file has already been selected:\n$file',
			toomuch: 'Выбранные изображении превышают максимально допустимый размер ($size)',
			toomany: 'Максимально допустимое количество изображений: $max',
			toobig: '$file is too big (max $size)'
		},
	});

	$("input[name='pledge_type']").on('change', function(event) {
		if($(this).val() == 'auction')
			$('#auction-time-controls-block').show();
		else
			$('#auction-time-controls-block').hide();
	});
});