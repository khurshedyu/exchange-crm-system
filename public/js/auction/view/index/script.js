$(function() {

	$('.countdown').each(function() {

		var auctionBlock = $(this).parent();
		var cntDownTime = new Date($(this).data('date-countdown').replace(/-/g, "/"));

		$(this).countdown(cntDownTime)
		.on('update.countdown', function(event) {

			if(event.offset.hours == 0) {
				$('.disable-panel', auctionBlock).css('display', 'none');
			} 

			if(event.offset.hours == 1) {
				if($('.disable-panel', auctionBlock).length == 0)
					$(auctionBlock).prepend('<div class="disable-panel"><h1 class="precountdown"></h1></div>');
				else {
					$('.disable-panel .precountdown', auctionBlock).html(event.strftime('%S'));	
				}

				if(event.offset.seconds<4) {
					$('.disable-panel .precountdown', auctionBlock).fadeTo(0, 0.0).fadeTo(1000, 1.0);
					if(event.offset.seconds == 0)
						$('.disable-panel .precountdown', auctionBlock).stop(true).fadeTo(200, 1.0);
				}	
			} else {
				if(event.offset.minutes==0 && event.offset.seconds<10)
					$(this).fadeTo(0, 0.0).fadeTo(1000, 1.0);

				$(this).html(event.strftime('%H:%M:%S'));
			}
		})
		.on('finish.countdown', function(event) {
			
			$.ajax({
				url: 'ajax/auction/pledge/close',
				type: 'POST',
				data: {item_id: $(auctionBlock).data('item-id')},
			});

			$(this).html('&nbsp');
			$(this).stop(true).fadeTo(0, 1.0);
			
			var completeHtml = '<i class="fa fa-times fa-2x close-lot-btn"></i><h1 class="final-panel-h1">' + $(auctionBlock).find('.lot').text() + '</h1 class="final-panel-h1"><h1 class="final-panel-h1">Продано за:</h1><h1 class="final-panel-h1">' + $(auctionBlock).find('.initial-price').text() + '</h1>';

			if($('.disable-panel', auctionBlock).length == 0)
				$(auctionBlock).prepend('<div class="disable-panel">' + completeHtml + '</div>');
			else
				$('.disable-panel', auctionBlock).html(completeHtml);

			$('.close-lot-btn', auctionBlock).bind('click', function() {
				$(auctionBlock).remove();
			});

			disableAuctionElems(auctionBlock, true, true);
		});
	});

	$(".stepper-input").each(function(index, val) {
		 $(this).TouchSpin({
		 	initval: $(this).data('initval') + (Math.ceil($(this).data('initval')/100)),
		 	min: $(this).data('initval') + (Math.ceil($(this).data('initval')/100)),
			verticalupclass: 'glyphicon glyphicon-plus',
			verticaldownclass: 'glyphicon glyphicon-minus',
			prefix: '$',
		 	verticalbuttons: true,
		 	mousewheel: false,
		 	booster: false,
			step: Math.ceil($(this).data('initval')/100)
		 });
	});

	window.auction = {};
	auction.BrainSocket = new BrainSocket(
		// new WebSocket('ws://69.28.90.247:8081'),
		new WebSocket('ws://localhost:8081'),
		new BrainSocketPubSub()
	);

	auction.BrainSocket.Event.listen('toclient.newprice',function(msg) {
		var serverData = msg.server.data;
		
		var itemBlock = $('div').find("[data-item-id='" + serverData.item_id + "']");
		var priceTchSpin = $(itemBlock).find('.stepper-input');
		var prevPrice = $(itemBlock).find('.initial-price');

		$(itemBlock).find('.auction-item-header').css('backgound-color', 'red');
		$(prevPrice).html('$' + serverData.new_price);
		$(itemBlock).find('.auction-item-header').animate( { backgroundColor: 'violet' }, 100);

		$(priceTchSpin).trigger("touchspin.updatesettings", {
			min: parseInt(serverData.minPrice),
		});
		
		serverData.newCountdownTime.date = serverData.newCountdownTime.date.substr(0,19);
		var cntDownTime = new Date(serverData.newCountdownTime.date.replace(/-/g, "/"));
		
		$(itemBlock).find('.countdown').countdown(cntDownTime, function(event) {
			$(this).html(event.strftime('%H:%M:%S'));
		}).on('finish.countdown', function(event) {
			$(this).closest('.item-block').remove();
		});
	});

	$('body').on('click', '.request-btn', function() {
		var auctionElems = $(this).closest('.item-block');
		var new_price = $(this).closest('div.item-block').find('.stepper-input');
		var item_id = $(this).closest('div.item-block').data('item-id');
		var countdown_item = $(this).closest('div.item-block').find('.countdown');

		disableAuctionElems(auctionElems,true, false);
		$.ajax({
			url: '/ajax/auction/pledge/newrequest',
			type: 'POST',
			data: {'new_price': $(new_price).val(), 'item_id': item_id, 'user_id': user_id, 'next_bit': $(new_price).data('initval')/100},
		})
		.done(function(data) {
			if(data == 'true') {
				auction.BrainSocket.message('toserver.newrequest',{'new_price': $(new_price).val(), 'item_id': item_id, 'user_id': user_id, 'next_bit': $(new_price).data('initval')/100});
				disableAuctionElems(auctionElems, false, false);
			}
		});
	});

	function disableAuctionElems(auctionElems, disable, isEnd) {
		if(disable && !isEnd) {
			$(auctionElems).find('.request-block button, .request-block a, .bootstrap-touchspin input').toggleClass('disabled');
			$(auctionElems).find('.auction-item-header').append('<i class="fa fa-circle-o-notch fa-spin bet-spinner" style="font-size: 17px; margin-left: 10px; color: #3BAFDA;"></i>');
		}
		else if(!disable && !isEnd) {
			$(auctionElems).find('.request-block button, .request-block a, .bootstrap-touchspin input').toggleClass('disabled');
			$(auctionElems).find('.auction-item-header .bet-spinner').remove();
		}
		else if(disable && isEnd) {
			// $('body').off('click', $('.request-block button', auctionElems)).off('click');
		}
	}
});