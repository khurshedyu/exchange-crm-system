$(function() {
	$('.offer-btns a.decline').on('click', function() {
		
		var offerBlock = $(this).closest('div.panel-default');

		$.ajax({
			url: "ajax/offer/delete",
			type: 'POST',
			data: {offer_id: $(offerBlock).data('offer-id'), status: $(offerBlock).data('offer-status'), module_id_from: $(offerBlock).data('module-id-from'), module_id_to: $(offerBlock).data('module-id-to'), module: $(offerBlock).data('module')},
		})
		.done(function(data) {
			console.log(data);
			if(data == 'deleted') {
				$(offerBlock).remove();
				if($('#offers-container').children('.alert').length == 0 && $('#offers-container').children('.panel-default').length == 0)
					window.location.replace(prevUrl);
			}
		})
		
	});

	$('.offer-btns a.accept').on('click', function() {
		var offerBlock = $(this).closest('div.panel-default');

		$.ajax({
			url: "ajax/offer/accept",
			type: 'POST',
			data: {offer_id: $(offerBlock).data('offer-id'), status: $(offerBlock).data('offer-status'), module_id_from: $(offerBlock).data('module-id-from'), module_id_to: $(offerBlock).data('module-id-to'), module: $(offerBlock).data('module')},
		})
		.done(function(data) {
			if(data.status == 'accepted') {
				$(offerBlock).after('<div class="alert alert-success alert-dismissable offer-alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Принято.</strong> Мы отправили уведомление. Ответ поступит в ближайщее время.</div>');
			} else if(data.status == 'invalid') {

				$(offerBlock).after('<div class="alert alert-danger alert-dismissable offer-alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>' + data.message + '</div>');

				$.each(data.invalidOffers, function(index, val) {
					$("div.panel-default[data-offer-id='" + val + "']").remove();
				});

			} else if(data.status == 'finish') {
				$(offerBlock).after('<div class="alert alert-success alert-dismissable offer-alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> ' + data.successMsg + ' </div>');
			}

			$(offerBlock).remove();

			$('.offer-alert').on('closed.bs.alert', function () {
				if($('#offers-container').children('.alert').length == 0 && $('#offers-container').children('.panel-default').length == 0)
					window.location.replace(prevUrl);
			});

		});
	});
});
