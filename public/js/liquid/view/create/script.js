$(function() {
	$('#duration_range').ionRangeSlider({
		min: 1,
		grid: true,
		postfix: " дней",
		hide_min_max: true,
		from: 6,
		onChange: function(data) {
			$('.irs-single').html(monthToDateStr(data.from));
		},
		onUpdate: function(data) {
			$('.irs-single').html(monthToDateStr(data.from));
		},
		onStart: function(data) {
			$('.irs-single').html(monthToDateStr(data.from));
		},
		onFinish: function(data) {
			$('.irs-single').html(monthToDateStr(data.from));
		}
	});

	$(".upload-btn").click(function () {
		$("#pledge-images").trigger('click');
	});

	if($('#pledge_type :selected').val() == '') {
		$('#pledge-controls-block').hide();
	}

	$('#pledge_type').on('change', function() {
		if($(this).val() != '')
			$('#pledge-controls-block').show();
		else
			$('#pledge-controls-block').hide();
	});

	$('#pledge-images').MultiFile({
		accept: 'gif|jpg|png',
		max:10,
		STRING: {
			remove: 'x',
			denied: 'Вы не можете использовать $ext формат.\nИспользуйте следующие форматы: "*jpg, *png, *gif".',
			file: '$file',
			selected: 'File selected: $file',
			duplicate: 'This file has already been selected:\n$file',
			toomuch: 'Выбранные изображении превышают максимально допустимый размер ($size)',
			toomany: 'Максимально допустимое количество изображений: $max',
			toobig: '$file is too big (max $size)'
		},
	});

	$('#pledge_details').summernote({
		minHeight: 100,
		maxHeight: 500,
		height: 200,
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'hr']],
			['view', ['fullscreen', 'codeview']]
		],
	});

	$('textarea[name="pledge_details_content"]').html($('#pledge_details').code());

 	if(errorsCount>0) {
		var durationRange = $('#duration_range').data("ionRangeSlider");
		durationRange.update({
			from: duration
		});

		$('textarea[name="pledge_details_content"]').html(pledgeDetails);
	}
});



function monthToDateStr(from) {
	var durationVal = 0;
	if(from == 1) {
		durationVal = from + ' день';
	} else if(from > 1 && from < 5) {
		durationVal = from + ' дня';
	} else if(from >= 5) {
		durationVal = from + ' дней';
	}

	return durationVal;
}