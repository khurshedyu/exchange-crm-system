<?php

class CreditController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($type = null)
	{	
		if($type != null && $type == 'request') {
			$request_credit = Credit::where('type', 'request')->get();
			return View::make('modules.credits.index', array('type' => 'request'));
		}
		else
			return View::make('modules.credits.index');
	}


	public function getCredit() {

		if(Input::get("min") == null && Input::get("max") == null) {
			$credits = Credit::join('users', 'credits.user_id', '=', 'users.id')->select(array('credits.id', 'credits.sum', 'credits.interest_rate', 'credits.duration', 'credits.created_at', 'credits.currency_type'))->where('credits.type', 'credit');
		} elseif(Input::get("min") != '' && Input::get("max") == '') {
			$credits = Credit::sjoin('users', 'credits.user_id', '=', 'users.id')->elect(array('credits.id', 'credits.sum', 'credits.interest_rate', 'credits.duration', 'credits.created_at', 'credits.currency_type'))->where('credits.type', 'credit')->where('credits.sum', '>', Input::get('min'));
		} else
			$credits = Credit::join('users', 'credits.user_id', '=', 'users.id')->select(array('credits.id', 'credits.sum', 'credits.interest_rate', 'credits.duration', 'credits.created_at', 'credits.currency_type'))->where('credits.type', 'credit')->whereBetween('credits.sum', array(Input::get("min"), Input::get("max")));

		return Datatables::of($credits)
			->make(true);
	}

	public function getCreditRequest() {
		if(Input::get("min") == null && Input::get("max") == null) {
			$request = Credit::join('users', 'credits.user_id', '=', 'users.id')->select(array('credits.id', 'credits.sum', 'credits.interest_rate', 'credits.duration', 'credits.created_at', 'credits.currency_type', 'credits.pledge'))->where('credits.type', 'request');
		} elseif(Input::get("min") != '' && Input::get("max") == '') {
			$request = Credit::join('users', 'credits.user_id', '=', 'users.id')->select(array('credits.id', 'credits.sum', 'credits.interest_rate', 'credits.duration', 'credits.created_at', 'credits.currency_type', 'credits.pledge'))->where('credits.type', 'request')->where('credits.sum', '>', Input::get('min'));
		} else
			$request = Credit::join('users', 'credits.user_id', '=', 'users.id')->select(array('credits.id', 'credits.sum', 'credits.interest_rate', 'credits.duration', 'credits.created_at', 'credits.currency_type', 'credits.pledge'))->where('credits.type', 'request')->whereBetween('credits.sum', array(Input::get("min"), Input::get("max")));

		return Datatables::of($request)
			->add_column('more_info', '<a href="/credit/request/details/{{$id}}" class="btn btn-primary btn-block btn-sm">Подробнее</a>')
			->add_column('new_request', '<button type="button" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#new-request-modal" data-credit-id="{{$id}}">Предложить</button>')
			->make(true);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($type = null)
	{
		if($type != null && $type == 'request')
			return View::make('modules.credits.create', array('type' => 'request'));
		else
			return View::make('modules.credits.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'sum'			=> 'required|numeric',
			'pledge_type'	=> 'required',
			'currency_type' => 'required',
			'interest_rate'	=> 'required|numeric'
		);

		$friendy_names = array(
			'sum' => '"Предлогаемая Цена"',
			'interest_rate' => '"Годовая Процентная Ставка"',
			'pledge_type' => '"Тип Залога"',
			'currency_type' => '"Валюта"',
		);

		if(Input::exists('pledge')) {			
			$rules = $rules + array('pledge' => 'required', 'pledge_details_content' => 'required');
			$friendy_names = $friendy_names + array('pledge' => '"Залог"', 'pledge_details_content' => '"Описание Залога"');
		}

		if(strpos(Input::get('interest_rate'), ',') !== false)
				Input::merge(array('interest_rate' => str_replace(',', '.', Input::get('interest_rate'))));

		$validator = Validator::make(Input::all(), $rules);
		$validator->setAttributeNames($friendy_names);

		if($validator->fails())
				return Redirect::back()->withErrors($validator)->withInput(Input::all());
		 else {

			$credit = new Credit;
			$credit->user_id = Auth::user()->id;
			$credit->type = 'credit';
			$credit->sum = Input::get('sum');
			$credit->interest_rate = Input::get('interest_rate');
			$credit->currency_type = Input::get('currency_type');
			$credit->duration = Input::get('duration');
			$credit->pledge_type = Input::get('pledge_type');

			if(Input::exists('pledge')) {

				DB::transaction(function() use($credit){

					$credit->type = 'request';
					$credit->pledge = Input::get('pledge');
					$credit->pledge_description = trim(Input::get('pledge_details_content'));
					$credit->save();

					Event::fire('credit.offer.newrecord', array($credit));

					// if(Input::get('pledge_type') == 'Твердый залог') {
						$files = Input::file('pledge_photo');
						if($files[0] != null) {
							foreach($files as $file) { 
								$destinationPath = public_path() . '/pledge-images/';
								$file_parts = pathinfo($file->getClientOriginalName());
								$original_name = $file_parts['filename'];
								$filename = $file_parts['filename'] . '_' . time() .'.'. $file_parts['extension'];

								$pledgePhoto = new ModulePhoto;
								$pledgePhoto->credit_id = $credit->id;
								$pledgePhoto->title = $original_name;
								$pledgePhoto->filename = $filename;
								$pledgePhoto->save();

								$upload_success = $file->move($destinationPath, $filename);
							}
						}
					// }
				});

				return Redirect::home()->with('success-message', 'Кредит успешно добавлен!');

			} else {

				$credit->save();
				Event::fire('credit.offer.newrecord', array($credit));
				
				return Redirect::home()->with('success-message', 'Кредит успешно добавлен!');
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	public function showRequest($id)
	{
		$request = Credit::find($id);
		$request_photos = ModulePhoto::where('credit_id', $id)->get();

		return View::make('modules.credits.request.details', array('request' => $request, 'request_photos' => $request_photos));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}
