<?php

class CreditRequestsController extends \BaseController {

	/**
	 * Display a listing of creditrequests
	 *
	 * @return Response
	 */
	public function index()
	{
		$creditrequests = Creditrequest::all();

		return View::make('creditrequests.index', compact('creditrequests'));
	}

	/**
	 * Show the form for creating a new creditrequest
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('creditrequests.create');
	}

	/**
	 * Store a newly created creditrequest in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$creditRequest = new CreditRequest;
		if(Input::exists('liquid_id'))
			$creditRequest->liquid_id = Input::get('liquid_id');
		else
			$creditRequest->credit_id = Input::get('credit_id');

		$creditRequest->user_id = Auth::id();
		
		if(Input::has('sum'))
			$creditRequest->sum = Input::get('sum');

		if(Input::has('interest_rate'))
			$creditRequest->interest_rate = Input::get('interest_rate');

		$creditRequest->duration = Input::get('duration');
		$creditRequest->pledge_type = Input::get('pledge_type');
		$creditRequest->save();

		if(Input::exists('liquid_id'))
			return Redirect::to('/liquidity/request')->with('success-message', 'Предложение отправлено!');
		else
			return Redirect::to('/credit/request')->with('success-message', 'Предложение отправлено!');
	}

	/**
	 * Display the specified creditrequest.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$creditrequest = Creditrequest::findOrFail($id);

		return View::make('creditrequests.show', compact('creditrequest'));
	}

	/**
	 * Show the form for editing the specified creditrequest.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$creditrequest = Creditrequest::find($id);

		return View::make('creditrequests.edit', compact('creditrequest'));
	}

	/**
	 * Update the specified creditrequest in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$creditrequest = Creditrequest::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Creditrequest::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$creditrequest->update($data);

		return Redirect::route('creditrequests.index');
	}

	/**
	 * Remove the specified creditrequest from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Creditrequest::destroy($id);

		return Redirect::route('creditrequests.index');
	}

}
