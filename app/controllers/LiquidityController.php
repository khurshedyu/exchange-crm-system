<?php

class LiquidityController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($type = null)
	{
		if($type != null && $type == 'request') {
			$request_credit = Liquidity::where('type', 'request')->get();
			return View::make('modules.liquidity.index', array('type' => 'request'));
		}
		else
			return View::make('modules.liquidity.index');
	}

	public function getLiquid() {

		if(Input::get("min") == null && Input::get("max") == null) {
			$liquids = Liquidity::join('users', 'liquidity.user_id', '=', 'users.id')->select(array('liquidity.id', 'users.company_name', 'liquidity.sum', 'liquidity.interest_rate', 'liquidity.duration', 'liquidity.created_at', 'users.phone'))->where('liquidity.type', 'credit');
		} elseif(Input::get("min") != '' && Input::get("max") == '') {
			$liquids = Liquidity::join('users', 'liquidity.user_id', '=', 'users.id')->select(array('liquidity.id', 'users.company_name', 'liquidity.sum', 'liquidity.interest_rate', 'liquidity.duration', 'liquidity.created_at', 'users.phone'))->where('liquidity.type', 'credit')->where('liquidity.sum', '>', Input::get('min'));
		} else
			$liquids = Liquidity::join('users', 'liquidity.user_id', '=', 'users.id')->select(array('liquidity.id', 'users.company_name', 'liquidity.sum', 'liquidity.interest_rate', 'liquidity.duration', 'liquidity.created_at', 'users.phone'))->where('liquidity.type', 'credit')->whereBetween('liquidity.sum', array(Input::get("min"), Input::get("max")));

		return Datatables::of($liquids)
			->add_column('contact_info', '<a tabindex="0" class="btn btn-sm btn-primary btn-block more-info-popover" role="button" data-placement="top" data-toggle="popover" data-trigger="focus" title="Контактная информация" data-content="Тел: {{ $phone }}"><i class="fa fa-info-circle fa-lg"></i></a>')
			->make(true);
	}

	public function getLiquidRequest() {
		if(Input::get("min") == null && Input::get("max") == null) {
			$request = Liquidity::join('users', 'liquidity.user_id', '=', 'users.id')->select(array('liquidity.id', 'users.company_name', 'liquidity.sum', 'liquidity.duration', 'liquidity.pledge', 'liquidity.created_at', 'users.phone'))->where('liquidity.type', 'request');
		} elseif(Input::get("min") != '' && Input::get("max") == '') {
			$request = Liquidity::join('users', 'liquidity.user_id', '=', 'users.id')->select(array('liquidity.id', 'users.company_name', 'liquidity.sum', 'liquidity.duration', 'liquidity.pledge', 'liquidity.created_at', 'users.phone'))->where('liquidity.type', 'request')->where('liquidity.sum', '>', Input::get('min'));
		} else
			$request = Liquidity::join('users', 'liquidity.user_id', '=', 'users.id')->select(array('liquidity.id', 'users.company_name', 'liquidity.sum', 'liquidity.duration', 'liquidity.pledge', 'liquidity.created_at', 'users.phone'))->where('liquidity.type', 'request')->whereBetween('liquidity.sum', array(Input::get("min"), Input::get("max")));

		return Datatables::of($request)
			->add_column('contact_info', '<a tabindex="0" class="btn btn-sm btn-primary btn-block more-info-popover" role="button" data-placement="top" data-toggle="popover" data-trigger="focus" title="Контактная информация" data-content="Тел: {{ $phone }}"><i class="fa fa-info-circle fa-lg"></i></a>')
			->add_column('more_info', '<a href="/liquidity/request/details/{{$id}}" class="btn btn-primary btn-block btn-sm">Подробнее</a>')
			->add_column('new_request', '<button type="button" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#new-request-modal" data-liquid-id="{{$id}}">Предложить</button>')
			->make(true);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($type = null)
	{
		if($type != null && $type == 'request')
			return View::make('modules.liquidity.create', array('type' => 'request'));
		else
			return View::make('modules.liquidity.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'sum'			=> 'required|numeric',
			'pledge_type'	=> 'required',
			'currency_type' => 'required',
			'interest_rate'	=> 'required|numeric',
		);

		$friendy_names = array(
			'sum' => '"Предлогаемая Цена"',
			'interest_rate' => '"Годовая Процентная Ставка"',
			'pledge_type' => '"Тип Залога"',
			'currency_type' => '"Валюта"',
		);

		if(Input::exists('pledge')) {			
			$rules = $rules + array('pledge' => 'required', 'pledge_details_content' => 'required');
			$friendy_names = $friendy_names + array('pledge' => '"Залог"', 'pledge_details_content' => '"Описание Залога"');
		}

		if(strpos(Input::get('interest_rate'), ',') !== false) {
			Input::merge(array('interest_rate' => str_replace(',', '.', Input::get('interest_rate'))));
		}

		$validator = Validator::make(Input::all(), $rules);
		$validator->setAttributeNames($friendy_names);

		if($validator->fails())
				return Redirect::back()->withErrors($validator)->withInput(Input::all());
		 else {

			$liquid = new Liquidity;
			$liquid->user_id = Auth::user()->id;
			$liquid->type = 'liquid';
			$liquid->sum = Input::get('sum');
			$liquid->interest_rate = Input::get('interest_rate');
			$liquid->currency_type = Input::get('currency_type');
			$liquid->pledge_type = Input::get('pledge_type');
			$liquid->duration = Input::get('duration');

			if(Input::exists('pledge')) {

				DB::transaction(function() use($liquid){
					$liquid->type = 'request';
					$liquid->pledge = Input::get('pledge');
					$liquid->pledge_description = trim(Input::get('pledge_details_content'));
					
					$liquid->save();
					Event::fire('liquid.offer.newrecord', array($liquid));

					$files = Input::file('pledge_photo');
					if($files[0] != null) {
						foreach($files as $file) {
							$destinationPath = public_path() . '/pledge-images/';
							$file_parts = pathinfo($file->getClientOriginalName());
							$original_name = $file_parts['filename'];
							$filename = $file_parts['filename'] . '_' . time() .'.'. $file_parts['extension'];
							$pledgePhoto = new ModulePhoto;
							$pledgePhoto->liquid_id = $liquid->id;
							$pledgePhoto->title = $original_name;
							$pledgePhoto->filename = $filename;
							$pledgePhoto->save();

							$upload_success = $file->move($destinationPath, $filename);
						}
					}
				});

				return Redirect::home()->with('success-message', 'Кредит успешно добавлен!');
			} else {

				$liquid->save();
				Event::fire('liquid.offer.newrecord', array($liquid));

				return Redirect::home()->with('success-message', 'Кредит успешно добавлен!');
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	public function showRequest($id)
	{
		$request = Liquidity::find($id);
		$request_photos = ModulePhoto::where('liquid_id', $id)->get();

		return View::make('modules.liquidity.request.details', array('request' => $request, 'request_photos' => $request_photos));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
