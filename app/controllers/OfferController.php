<?php 

class OfferController extends \BaseController {


	public function deleteOffer() {

		if(Input::get('status') == 1) {
			$this->forceDestroy(Input::get('offer_id'));
			return Response::json('deleted');
		}
		elseif(Input::get('status') == 2) {
			
			$unsuccessOperation = Offer::find(Input::get('offer_id'));

			Operation::create(
				array(
					'module_id_from' => $unsuccessOperation->module_id_from,
					'module_id_to' => $unsuccessOperation->module_id_to,
					'module' => Input::get('module'),
					'from_user_id' => $unsuccessOperation->from_user_id,
					'to_user_id' => $unsuccessOperation->to_user_id,
					'offers_type' => $unsuccessOperation->offers_type,
					'offer_sum' => $unsuccessOperation->offer_sum,
					'offer_rate_from' => $unsuccessOperation->offer_rate_from,
					'offer_rate_to' => $unsuccessOperation->offer_rate_to,
					'currency_type' => $unsuccessOperation->currency_type,
					'currency_type_to' => $unsuccessOperation->currency_type_to,
					'status' => '0'
			));

			Offer::deleteOffersAndModuleRecord(Input::get('offer_id'), Input::get('module_id_from'), Input::get('module'));
			return Response::json('deleted');
		}
	}

	public function acceptOffer() {

		if(Input::get('status') == 1) {

			$currOffer = Offer::find(Input::get('offer_id'))->where('status', 0)->get();

			if($currOffer != null) {
				
				Offer::find(Input::get('offer_id'))->update(array('status' => Input::get('status')));
				Offer::where('id', '!=', Input::get('offer_id'))
					->where('module_id_to', Input::get('module_id_to'))
					->where('status', 0)
					->where('module', Input::get('module'))
					->forceDelete();

				return Response::json(array('status' => 'accepted'));

			} elseif ($currOffer == null) {

				$allInvalidOffers = Offer::where('to_user_id', Auth::id())
					->where('id', '!=', Input::get('offer_id'))
					->where('status', '-1')->lists('id');

				return Response::json(array('status' => 'invalid', 'message' => 'К сожалению эта запись недействительна.', 'invalidOffers' => $allInvalidOffers));
			}
		}
		elseif(Input::get('status') == 2) {

			$successOperation = Offer::find(Input::get('offer_id'));
			Offer::successOffer($successOperation);

			if($successOperation->module == 'currency') 
				$successMsg =  '<strong>Спасибо.</strong> Обмен произведен.';
			elseif($successOperation->module == 'credit.request' || $successOperation->module == 'liquid.request')
				$successMsg =  '<strong>Спасибо.</strong> Кредит получен.';
			elseif($successOperation->module == 'credit.credit' || $successOperation->module == 'liquid.liquid')
				$successMsg =  '<strong>Спасибо.</strong> Кредит предоставлен.';

			return Response::json(array('status' => 'finish', 'successMsg' => $successMsg));

		}
	}

	//delete offer
	public function forceDestroy($id)
	{
		Offer::find($id)->forceDelete();
	}


	public function destroy($id)
	{
		Offer::find($id)->delete();
	}

	public static function getCurrencyTypeWithSum($currencyType, $sum = null) {
		$currencyTypes = array('USD' => '$', 'EUR' => '&#8364;', 'GBP' => '&#163;', 'RUB' => '₽', 'TJS' => 'Cомони');

		if($sum == null)
			return $currencyTypes[$currencyType];

		if($currencyType == 'TJS')
			return $sum . ' ' . $currencyTypes[$currencyType];
		else {
			echo $currencyTypes[$currencyType];
			echo $sum;
		}

	}
}