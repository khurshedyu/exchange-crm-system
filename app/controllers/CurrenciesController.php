<?php

class CurrenciesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /currencies
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('modules.currency.index');
	}


	public function getCurrencies() {

		if(Input::get("currency_type") == null) {
			$liquids = Currency::join('users', 'currencies.user_id', '=', 'users.id')->select(array('currencies.id', 'currencies.sum', 'currencies.exchange_rate', 'currencies.type', DB::raw('concat(type_from, "/", type_to) as exchange_type, type_from, currencies.created_at'), 'currencies.user_id'));
		} elseif(Input::get("currency_type") != '') {
			$liquids = Currency::join('users', 'currencies.user_id', '=', 'users.id')->select(array('currencies.id', 'currencies.sum', 'currencies.exchange_rate', 'currencies.type', DB::raw('concat(type_from, "/", type_to) as exchange_type, type_from, currencies.created_at'), 'currencies.user_id'))->where('currencies.type_from', Input::get('currency_type'))->orWhere('currencies.type_to', Input::get('currency_type'));
		}
		
		return Datatables::of($liquids)
			->add_column('acquire', '<a href="#" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#acquire-modal" data-currency-id="{{$id}}">Приобрести</a>')
			->make(true);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /currencies/create
	 *
	 * @return Response
	 */
	public function create($type = null)
	{
		if($type != null && $type == 'buying')
			return View::make('modules.currency.create', array('type' => 'buying'));
		else
			return View::make('modules.currency.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /currencies
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'sum' => 'required|numeric',
			'type_from' => 'required|different:type_to',
			'type_to' => 'required|different:type_from',
			'exchange_rate' => 'required|numeric'
		);

		$friendly_names = array(
			'sum' => '"Продаю"',
			'type_from' => '"Валюта"',
			'type_to' => '"В обмен на"',
			'exchange_rate' => '"По курсу"'
		);

		if(strpos(Input::get('exchange_rate'), ',') !== false)
			Input::merge(array('exchange_rate' => str_replace(',', '.', Input::get('exchange_rate'))));

		$validator = Validator::make(Input::all(), $rules);
		$validator->setAttributeNames($friendly_names);

		if($validator->fails()) {
			return Redirect::back()
				->withInput(Input::all())		
				->withErrors($validator);
		} else {

			if(Input::exists('buying'))
				$type = 'Куплю';
			else
				$type = 'Продам';

			//Пользователь не может одновременно создать несколько одинаковых записей.
			// $identyCurr = Currency::where('user_id', Auth::id())
			// 	->where('type', $type)
			// 	->where('type_from', Input::get('type_from'))
			// 	->where('type_to', Input::get('type_to'))
			// 	->where('sum', Input::get('sum'))
			// 	->where('exchange_rate', Input::get('exchange_rate'));

			// if($identyCurr->count()>=1)
			// 	return Redirect::back()->with('warning-message', 'Ранее вы запрашивали эту сумму!');

			$currency = new Currency;
			$currency->user_id = Auth::id();

			if($type == 'Куплю') {
				$currency->type = $type;
				$currency->exchange_rate_fees = Input::get('exchange_rate') - Config::get('app.fees.currency.rate');
			}
			else {
				$currency->type = $type;
				$currency->exchange_rate_fees = Input::get('exchange_rate') + Config::get('app.fees.currency.rate');
			}

			$currency->type_from = Input::get('type_from');
			$currency->type_to = Input::get('type_to');
			$currency->sum = Input::get('sum');
			$currency->exchange_rate = Input::get('exchange_rate');
			$currency->save();

			Event::fire('currency.offer.newrecord', array($currency));

			return Redirect::home()->with('success-message', 'Валюта успешно добавлена!');
		}
	}

	/**
	 * Display the specified resource.
	 * GET /currencies/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /currencies/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /currencies/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /currencies/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}