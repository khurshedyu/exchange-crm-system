<?php

class RegistrationController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /registration
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('registration.index');
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /registration/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /registration
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'first_name' => 'required|alpha',
			'last_name' => 'required|alpha',
			'patronymic' => 'required|alpha',
			'email' => 'required|email|unique:users',
			'password' => 'required|alphanum',
			'confirm_password' => 'required|same:password',
			'phone' => 'required',
			'captcha' => 'required|captcha'
		);

		$messages = array(
			'captcha' => 'Символы введены неправильно. Повторите.'
		);

		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->fails()) {
			return Redirect::back()
				->withErrors($validator)
				->withInput(Input::except(['password', 'confirm_password']));
		} else {


			$username = Input::get('first_name') . ' ' . Input::get('last_name');

			//Activation code
			$code = str_random(60);

			$user = new User;
			$user->first_name = Input::get('first_name');
			$user->last_name = Input::get('last_name');
			$user->patronymic = Input::get('patronymic');
			$user->email = Input::get('email');
			$user->password = Hash::make(Input::get('password'));
			$user->phone = Input::get('phone');
			$user->city = Input::get('city');
			$user->company_name = Input::get('company_name');
			$user->role = 'admin';
			$user->code = $code;
			$user->active = 0;
			$user->save();


			if($user) {
				Mail::send('emails.activation.activate', array('link' => URL::route('activate-account', $code), 'username' => $username), function($message) use ($user, $username) {
					$message->to($user->email, $username)->subject('Активация аккаунта');
				});

				return Redirect::to('/login')->with('success-message', 'Ваш аккаунт создан. Для активации вы должны перейти по ссылке которая отправлена на вашу эл. почту.');
			}
		}
	}


	public function getActivate($code) {
		$user = User::where('code', '=', $code)->where('active', '=', 0);

		if($user->count()) {
			$user = $user->first();

			//active user to active state
			$user->active = 1;
			$user->code = '';
			
			if($user->save()) {
				return Redirect::to('login')->with('success-message', 'Ваш аккаунт успешно активирован. Теперь вы можете войти в систему!');
			}
		}

		return Redirect::to('/login')->with('warning-message', 'Не получилось активировать ваш аккаунт. Попробуйте еще раз.');
	}


	/**
	*
	*
	*
	*/
	public function getRecoverView() {
		return View::make('registration.recover-form');
	}


	public function recoverPassSendMail() {

		$validator = Validator::make(Input::all(), array('email'=>'required|email|exists:users,email'));

		if($validator->fails()) {
			return Redirect::back()
				->withInput(Input::all())
				->withErrors($validator);
		} 
		else {
		
			$user = User::where('email', '=', Input::get('email'))->first();
			$username = $user->first_name . ' ' . $user->last_name;


			if($user->active == 0) {
				return Redirect::back()->with('warning-message', 'Пожалуйста для начала активируйте ваш аккаунт!');
			} elseif($user->active == 1 && $user->code != '')
				return Redirect::back()->with('warning-message', 'Вы ранее получили письмо об восстановлении пароля. Пожалуйста проверьте почту.');
			else {
				//chage password code
				$code = str_random(60);
				$user->code = $code;
				$user->save();

				Mail::send('emails.recovering.recover', array('link' => URL::route('recover-password', $code), 'username' => $username), function($message) use ($user, $username) {
					$message->to($user->email, $username)->subject('Смена пароля');
				});
				
				return Redirect::to('/login')->with('success-message', 'Для изменения пароля вы должны перейти по ссылке которая отправлена на вашу эл. почту.');
			}
		}
	}



	public function getRecoverPassView($code) {

		$user = User::where('code', '=', $code)->where('active', '=', 1);
		if($user->count())
			return View::make('registration.recover-password', array('code' => $code));
		else 
			return App::abort(404);
	}


	public function changeUserPassword ($code) {

		$validator = Validator::make(
			Input::all(),
			array('new_password' => 'required|alphanum', 'repeat_password' => 'required|same:new_password')
		);


		if($validator->fails()) {
			return Redirect::back()
				->withInput(Input::all())
				->withErrors($validator);
		} else {
			$user = User::where('code', '=', $code)->where('active', '=', 1);

			if($user->count()) {
				$user = $user->first();
				$user->password = Hash::make(Input::get('new_password'));
				$user->code = '';

				if($user->save()) {
					return Redirect::to('login')->with('success-message', 'Ваш пароль успешно изменен. Теперь вы можете войти в систему!');
				}
			}
		}


	}


	/**
	 * Display the specified resource.
	 * GET /registration/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /registration/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /registration/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /registration/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}