<?php

class AuctionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /auction
	 *
	 * @return Response
	 */
	public function index() {
		$pledges = Auction::join('users', 'auction.user_id', '=', 'users.id')
			->leftJoin('users_bet', 'users_bet.pledge_id', '=', 'auction.id')
			->selectRaw('max(users_bet.bet_price) as max_bet_price, auction.id, auction.user_id, auction.item, auction.start_price, users.company_name, auction.completion_at')
			->where('auction.status', 'opened')
			->where('auction.completion_at', '>', Carbon::now())
			->where('auction.completion_at', '<', Carbon::now()->addHour()->addSeconds(30))
			->orderBy('users_bet.created_at', 'desc')
			->groupBy('auction.id')->get();

		if(Request::ajax()) {
			return Response::json(View::make('modules.auction.pledges', array('pledges' => $pledges))->render());
		}

		return View::make('modules.auction.index', array('pledges' => $pledges));
	}

	public function indexFixedPledges() {
		return View::make('modules.auction.pledge.index');
	}


	public function getAuctionPledges() {
		$pledges = Auction::join('users', 'auction.user_id', '=', 'users.id')
		->leftJoin('users_bet', 'users_bet.pledge_id', '=', 'auction.id')
		->selectRaw('max(users_bet.bet_price) as max_bet_price, auction.id, auction.item, auction.start_price, users.company_name, auction.completion_at')
		->where('auction.status', 'opened')->where('auction.completion_at', '>', Carbon::now()->__toString())
		->where('auction.completion_at', '<', Carbon::now()->addHour()->addSeconds(10))
		->orderBy('users_bet.created_at', 'desc')
		->groupBy('auction.id');
		// ->paginate(2);

		return View::make('modules.auction.pledges')->with('pledges', $pledges)->render();
		// return View::make('modules.auction.index')->with('pledges', $pledges);
	}

	public function postNewRequest() {

		if(Request::ajax()) {
			$maxbet = UsersBet::where('pledge_id', Input::get('item_id'))->orderBy('bet_price', 'desc')->limit(1)->first();
			$auction_price = Auction::find(Input::get('item_id'))->start_price;
			
			if((($maxbet == null || $maxbet->count()==0) && $auction_price<Input::get('new_price')) || $maxbet->bet_price < Input::get('new_price')) {

				$newBet = new UsersBet;
				$newBet->user_id = Input::get('user_id');
				$newBet->pledge_id = Input::get('item_id');
				$newBet->bet_price = Input::get('new_price');
				$newBet->save();

				// $fiveMinutes = Carbon::now()->addMinutes(5);
				// Auction::find(Input::get('item_id'))->update(array('completion_at' => $fiveMinutes));

				return 'true';
			} else {
				return 'false';
			}

		}
	}

	public function closePledge() {
		if(Request::ajax()) {
			Auction::find(Input::get('item_id'))->update(array('status' => 'closed'));

		}
	}

	// public function getAuctionPledges() {
	// 	if(Input::get("min") == null && Input::get("max") == null) {
	// 		$pledges = Auction::join('users', 'auction.user_id', '=', 'users.id')->select(array('auction.id', 'users.company_name', 'auction.item', 'auction.start_price', 'users.phone', 'auction.completion_at'))->whereNotNull('completion_at');
	// 	} elseif(Input::get("min") != '' && Input::get("max") == '') {
	// 		$pledges = Auction::join('users', 'auction.user_id', '=', 'users.id')->select(array('auction.id', 'users.company_name', 'auction.item', 'auction.start_price', 'users.phone', 'auction.completion_at'))->whereNotNull('completion_at')->where('auction.start_price', '>', Input::get('min'));
	// 	} else
	// 		$pledges = Auction::join('users', 'auction.user_id', '=', 'users.id')->select(array('auction.id', 'users.company_name', 'auction.item', 'auction.start_price', 'users.phone', 'auction.completion_at'))->whereNotNull('completion_at')->whereBetween('auction.start_price', array(Input::get("min"), Input::get("max")));

	// 	return Datatables::of($pledges)
	// 		->add_column('contact_info', '<a tabindex="0" class="btn btn-sm btn-primary btn-block more-info-popover" role="button" data-placement="top" data-toggle="popover" data-trigger="focus" title="Контактная информация" data-content="Тел: {{ $phone }}"><i class="fa fa-info-circle fa-lg"></i></a>')
	// 		->add_column('countdown', '<div class="countdown" data-date-countdown="{{$completion_at}}"></div>')
	// 		->add_column('new_request', '<div class="input-group"><input class="form-control"> <span class="input-group-btn"><button type="submit" class="btn btn-primary" data-type="last"><span class="hidden-xs">Предложить</span><span class="visible-xs fa fa-info"></span></button></span></div>')
	// 		->add_column('more_info', '<a href="/auction/details/{{$id}}" class="btn btn-primary btn-block btn-sm">Подробнее</a>')
	// 		->make(true);
	// }

	public function getFixedPledges() {
		if(Input::get("min") == null && Input::get("max") == null) {
			$pledges = Auction::join('users', 'auction.user_id', '=', 'users.id')->select(array('auction.id', 'users.company_name', 'auction.item', 'auction.start_price', 'users.phone'))->whereNull('completion_at');
		} elseif(Input::get("min") != '' && Input::get("max") == '') {
			$pledges = Auction::join('users', 'auction.user_id', '=', 'users.id')->select(array('auction.id', 'users.company_name', 'auction.item', 'auction.start_price', 'users.phone'))->whereNull('completion_at')->where('auction.start_price', '>', Input::get('min'));
		} else
			$pledges = Auction::join('users', 'auction.user_id', '=', 'users.id')->select(array('auction.id', 'users.company_name', 'auction.item', 'auction.start_price', 'users.phone'))->whereNull('completion_at')->whereBetween('auction.start_price', array(Input::get("min"), Input::get("max")));

		return Datatables::of($pledges)
			->add_column('contact_info', '<a tabindex="0" class="btn btn-sm btn-primary btn-block more-info-popover" role="button" data-placement="top" data-toggle="popover" data-trigger="focus" title="Контактная информация" data-content="Тел: {{ $phone }}"><i class="fa fa-info-circle fa-lg"></i></a>')
			->add_column('more_info', '<a href="/pledges/details/{{$id}}" class="btn btn-primary btn-block btn-sm">Подробнее</a>')
			->make(true);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /auction/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('modules.auction.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /auction
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'item' => 'required',
			'start_price' => 'required|numeric'
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()) {
			return Redirect::back()
				->withInput(Input::all())
				->withErrors($validator);
		} else {
			DB::transaction(function() {

				$pledge = new Auction;
				$pledge->user_id = Auth::id();
				$pledge->item = Input::get('item');
				$pledge->start_price = Input::get('start_price');
				$pledge->item_details = trim(Input::get('item_details'));
				if(Input::get('pledge_type') == 'auction') {
					$completion_at = new Carbon(Input::get('start_time'));
					$pledge->status = 'opened';
					$pledge->completion_at = $completion_at->addHour()->subSeconds(00);
				}

				$pledge->save();

				$files = Input::file('pledge_photo');
				if($files[0] != null) {
					foreach($files as $file) { 
						$destinationPath = public_path() . '/pledge-images/';
						$file_parts = pathinfo($file->getClientOriginalName());
						$original_name = $file_parts['filename'];
						$filename = $file_parts['filename'] . '_' . time() .'.'. $file_parts['extension'];
						
						$pledgePhoto = new ModulePhoto;
						$pledgePhoto->auction_id = $pledge->id;
						$pledgePhoto->title = $original_name;
						$pledgePhoto->filename = $filename;
						$pledgePhoto->save();

						$upload_success = $file->move($destinationPath, $filename);
					}
				}
			});

			return Redirect::home()->with('success-message', 'Залог успешно добавлен!');
		}

	}

	/**
	 * Display the specified resource.
	 * GET /auction/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showPledgeDetails($id)
	{
		$pledge = Auction::whereNotNull('completion_at')->find($id);

		if($pledge == null)
			return Redirect::to('auction')->with('warning-message', 'Запись отсутствует');

		$pledge_photos = ModulePhoto::where('auction_id', $id)->get();

		return View::make('modules.auction.show', array('pledge' => $pledge, 'pledge_photos' => $pledge_photos));
	}

	public function showFixedPledgeDetails($id)
	{
		$pledge = Auction::whereNull('completion_at')->find($id);

		if($pledge == null)
			return Redirect::to('pledges')->with('warning-message', 'Запись отсутствует');

		$pledge_photos = ModulePhoto::where('auction_id', $id)->get();

		return View::make('modules.auction.pledge.show', array('pledge' => $pledge, 'pledge_photos' => $pledge_photos));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /auction/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /auction/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /auction/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}