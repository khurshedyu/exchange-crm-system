<?php

class LoginController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /login
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Display the login form
	 * GET /login
	 *
	 * @return Response
	 */
	public function create()
	{	
		if (Auth::check()) {
			return Redirect::to('/');
		} else {
			return View::make('login.index');
		}

	}

	/**
	 * POST /login
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), array('email' => 'required|email', 'password' => 'required'));

		if($validator->fails()) {
			return Redirect::back()
				->withInput(Input::all())
				->withErrors($validator);
		} else {
		
			$remember = (Input::has('remember')) ? true : false;
			$auth = Auth::attempt(array(
				'email' => Input::get('email'),
				'password' => Input::get('password'),
				'active' => 1
			), $remember);

			if($auth) {
				return Redirect::intended('/');
			} else {
				return Redirect::to('/login')->with('warning-message', 'Проблемы при входе. Свяжитесь с администратором.');
			}
		}
	}


	public function welcomePage() {
		return View::make('welcome');
	}

	/**
	 * Display the specified resource.
	 * GET /login/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /login/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /login/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Logout Auth user.
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();
		return Redirect::home();
	}

}