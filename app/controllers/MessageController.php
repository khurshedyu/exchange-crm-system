<?php 
class MessageController extends \BaseController {


	public function getCurrencyOperation($from, $to) {

	}


	public function showMessages() {
		$currencyOperation = Operation::where('module', 'currency')
			->where(function ($query) {
				$query->where('from_user_id', Auth::id())
					->orWhere('to_user_id', Auth::id());
			})
			->where('created_at', '>=', Carbon::today())
			->orderBy('created_at', 'desc')->get();

		$creditOperation = Operation::whereIn('module', array('credit.credit', 'credit.request'))
			->where(function ($query) {
				$query->where('from_user_id', Auth::id())
					->orWhere('to_user_id', Auth::id());
			})
			->where('created_at', '>=', Carbon::today())
			->orderBy('created_at', 'desc')->get();

		$liquidOperation = Operation::whereIn('module', array('liquid.liquid', 'liquid.request'))
			->where(function ($query) {
				$query->where('from_user_id', Auth::id())
					->orWhere('to_user_id', Auth::id());
			})
			->where('created_at', '>=', Carbon::today())
			->orderBy('created_at', 'desc')->get();

		return View::make('messages', array('currencyOperation' => $currencyOperation, 'creditOperation' => $creditOperation, 'liquidOperation' => $liquidOperation));
	}

	public function getCurrencyMessages() {
		$rangeDates = array(
			'today' => Carbon::today(),
			'1week' => Carbon::today()->subWeek(),
			'2week' => Carbon::today()->subWeeks(2),
			'1' => Carbon::today()->subMonth(),
			'3' => Carbon::today()->subMonths(3),
			'6' => Carbon::today()->subMonths(6),
			'12' => Carbon::today()->subYear()
		);

		$currencyOperation = Operation::where('module', 'currency')
			->where(function ($query) {
				$query->where('from_user_id', Auth::id())
					->orWhere('to_user_id', Auth::id());
			})
			->where('created_at', '>=', $rangeDates[Input::get('range_value')])
			->orderBy('created_at', 'desc')->get();

		if(Request::ajax()) {
			return Response::json(View::make('currency_messages', array('currencyOperation' => $currencyOperation))->render());
		}
	}

	public function getCreditMessages() {
		$rangeDates = array(
			'today' => Carbon::today(),
			'1week' => Carbon::today()->subWeek(),
			'2week' => Carbon::today()->subWeeks(2),
			'1' => Carbon::today()->subMonth(),
			'3' => Carbon::today()->subMonths(3),
			'6' => Carbon::today()->subMonths(6),
			'12' => Carbon::today()->subYear()
		);

		$creditOperation = Operation::whereIn('module', array('credit.credit', 'credit.request'))
			->where(function ($query) {
				$query->where('from_user_id', Auth::id())
					->orWhere('to_user_id', Auth::id());
			})
			->where('created_at', '>=', $rangeDates[Input::get('range_value')])
			->orderBy('created_at', 'desc')->get();

		if(Request::ajax()) {
			return Response::json(View::make('credit_messages', array('creditOperation' => $creditOperation))->render());
		}
	}

	public function getLiquidMessages() {
		$rangeDates = array(
			'today' => Carbon::today(),
			'1week' => Carbon::today()->subWeek(),
			'2week' => Carbon::today()->subWeeks(2),
			'1' => Carbon::today()->subMonth(),
			'3' => Carbon::today()->subMonths(3),
			'6' => Carbon::today()->subMonths(6),
			'12' => Carbon::today()->subYear()
		);

		$liquidOperation = Operation::whereIn('module', array('liquid.liquid', 'liquid.request'))
			->where(function ($query) {
				$query->where('from_user_id', Auth::id())
					->orWhere('to_user_id', Auth::id());
			})
			->where('created_at', '>=', $rangeDates[Input::get('range_value')])
			->orderBy('created_at', 'desc')->get();

		if(Request::ajax()) {
			return Response::json(View::make('liquid_messages', array('liquidOperation' => $liquidOperation))->render());
		}
	}


	public function printCurrencyPDF() {
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('freeserif', '', 12);
		$pdf->AddPage();
		$pdf->writeHTML(View::make('currency_messages', array('currencyOperation' => Session::get('messages.currency'), 'is_pdf' => true))->render(), true, false, false, false, '');
		$filename = storage_path() . '/currency.pdf';
		$pdf->output($filename, 'F');

		return Response::download($filename);
	}

	public function printCreditPDF() {
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('freeserif', '', 12);
		$pdf->AddPage();
		$pdf->writeHTML(View::make('credit_messages', array('creditOperation' => Session::get('messages.credit'), 'is_pdf' => true))->render(), true, false, false, false, '');
		$filename = storage_path() . '/currency.pdf';
		$pdf->output($filename, 'F');

		return Response::download($filename);
	}

	public function printLiquidPDF() {
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('freeserif', '', 12);
		$pdf->AddPage();
		$pdf->writeHTML(View::make('liquid_messages', array('liquidOperation' => Session::get('messages.liquid'), 'is_pdf' => true))->render(), true, false, false, false, '');
		$filename = storage_path() . '/currency.pdf';
		$pdf->output($filename, 'F');

		return Response::download($filename);
	}
}