@if($liquidOperation->count()>0)
	@foreach ($liquidOperation as $operation)
		@if($operation->status == 1)
			<div class="alert alert-success operation-alert" role="alert">
			@if($operation->from_user_id == Auth::id())
				<strong>{{ $operation->created_at }}: </strong>
				@if($operation->module == 'liquid.request')
					<strong>Получен Кредит: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@else
					<strong>Предоставлен Кредит: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@endif
				<strong>Годовая процентная ставка: </strong> {{ $operation->offer_rate_from }}
			@else
				<strong>{{ $operation->created_at }}: </strong>
				@if($operation->module == 'liquid.request')
					<strong>Предоставлен Кредит: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@else
					<strong>Получен Кредит: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@endif
				<strong>Годовая процентная ставка: </strong> {{ $operation->offer_rate_to }}
			@endif
			</div>
		@else
			<div class="alert alert-danger operation-alert" role="alert">
			@if($operation->from_user_id == Auth::id())
				<strong>{{ $operation->created_at }}: </strong>
				@if($operation->module == 'liquid.request')
					<strong>Кредит не получен: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@else
					<strong>Кредит не предоставлен: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@endif
				<strong>Годовая процентная ставка: </strong> {{ $operation->offer_rate_from }}
			@else
				<strong>{{ $operation->created_at }}: </strong>
				@if($operation->module == 'liquid.request')
					<strong>Кредит не предоставлен: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@else
					<strong>Кредит не получен: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@endif
				<strong>Годовая процентная ставка: </strong> {{ $operation->offer_rate_to }}
			@endif
			</div>
		@endif
	@endforeach
	{{ Session::reflash() }}
	{{ Session::flash('messages.liquid', $liquidOperation) }}
	@if(!isset($is_pdf))
		<a href="/messages/liquid/print/pdf">Распечатать</a>
	@endif
@else
	0 сообщений
@endif