@extends('layouts.master')
@section('body')
	<div class="row">
		<div id="registration-box" class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading"><h5 class="panel-title">Регистрация</h5></div>
				<div class="panel-body">
					{{ Form::open(array('route' => 'register.store')) }}

					<div class="form-group @if($errors->has('first_name')) has-error has-feedback @endif">
						@if($errors->has('first_name'))
							<label class="control-label" for="inputError2">{{ $errors->first('first_name') }}</label>
						@endif
						{{ Form::text('first_name', Input::old('first_name'), array('class' => 'form-control', 'placeholder' => 'Имя*')) }}
					</div>

					<div class="form-group @if($errors->has('last_name')) has-error has-feedback @endif">
						@if($errors->has('last_name'))
							<label class="control-label" for="inputError2">{{ $errors->first('last_name') }}</label>
						@endif
						{{ Form::text('last_name', Input::old('last_name'), array('class' => 'form-control', 'placeholder' => 'Фамилия*')) }}
					</div>

					<div class="form-group @if($errors->has('patronymic')) has-error has-feedback @endif">
						@if($errors->has('patronymic'))
							<label class="control-label" for="inputError2">{{ $errors->first('patronymic') }}</label>
						@endif
						{{ Form::text('patronymic', Input::old('patronymic'), array('class' => 'form-control', 'placeholder' => 'Отчество*')) }}
					</div>

					<div class="form-group @if($errors->has('email')) has-error has-feedback @endif">
						@if($errors->has('email'))
							<label class="control-label" for="inputError2">{{ $errors->first('email') }}</label>
						@endif
						{{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'Эл. Почта*')) }}
					</div>

					<div class="form-group @if($errors->has('password')) has-error has-feedback @endif">
						@if($errors->has('password'))
							<label class="control-label" for="inputError2">{{ $errors->first('password') }}</label>
						@endif
						{{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Пароль*')) }}
					</div>

					<div class="form-group @if($errors->has('confirm_password')) has-error has-feedback @endif">
						@if($errors->has('confirm_password'))
							<label class="control-label" for="inputError2">{{ $errors->first('confirm_password') }}</label>
						@endif
						{{ Form::password('confirm_password', array('class' => 'form-control', 'placeholder' => 'Повотрите пароль*')) }}
					</div>

					<div class="form-group @if($errors->has('phone')) has-error has-feedback @endif">
						@if($errors->has('phone'))
							<label class="control-label" for="inputError2">{{ $errors->first('phone') }}</label>
						@endif
						{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control', 'placeholder' => 'Номер телефона*')) }}
					</div>

					<div class="form-group">
						{{ Form::text('city', Input::old('city'), array('class' => 'form-control', 'placeholder' => 'Город')) }}
					</div>

					<div class="form-group">
						{{ Form::text('company_name', Input::old('company_name'), array('class' => 'form-control', 'placeholder' => 'Компания')) }}
					</div>

					<div class="form-group @if($errors->has('captcha')) has-error has-feedback @endif">
						<label class="control-label">{{ HTML::image(Captcha::img()) }}</label>
						@if($errors->has('captcha'))
							<label class="control-label" for="inputError2">{{ $errors->first('captcha') }}</label>
						@endif
						{{ Form::text('captcha', null, array('class' => 'form-control', 'placeholder' => 'Введите символы с картинки')) }}
					</div>
					
					<button type="submit" class="btn btn-success register-btn pull-right" value="signin">Регистрация</button>

					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop