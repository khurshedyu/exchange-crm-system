@extends('layouts.master')
@section('body')
	<div class="row">
		<div id="loginbox" class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading"><h5 class="panel-title">Изменение пароля</h5></div>
				<div class="panel-body">
					{{ Form::open(array('url' => '/register/acount/activate/' . $code)) }}

						@if($errors->has('new_password'))
							<label style="color:#da4453">{{ $errors->first('new_password') }}</label>
						@endif
						<div class="input-group form-group @if($errors->has('new_password')) has-error has-feedback @endif">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							{{ Form::password('new_password', array('class' => 'form-control', 'placeholder' => 'Новый Пароль')) }}
						</div>

						@if($errors->has('repeat_password'))
							<label style="color:#da4453">{{ $errors->first('repeat_password') }}</label>
						@endif
						<div class="input-group form-group @if($errors->has('repeat_password')) has-error has-feedback @endif">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							{{ Form::password('repeat_password', array('class' => 'form-control', 'placeholder' => 'Повторить Пароль')) }}
						</div>

						<div class="input-group pull-right">
							<button type="submit" class="btn btn-success" value="signin">Изменить</button>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop