@extends('layouts.master')
@section('body')
	<div class="row">
		<div id="loginbox" class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading"><h5 class="panel-title">Восстановление пароля</h5></div>
				<div class="panel-body">
					{{ Form::open(array('route' => 'register.restore.email.send')) }}
						@if($errors->has('email'))
							<label style="color:#da4453">{{ $errors->first('email') }}</label>
						@endif
						<div class="input-group form-group @if($errors->has('email')) has-error has-feedback @endif">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							{{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'Эл. почта')) }}
						</div>

						<div class="input-group pull-right">
							<button type="submit" class="btn btn-success" value="signin">Восстановить</button>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop