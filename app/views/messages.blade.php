@extends('layouts.master')
@section('body')
<div class="row row-bottom-offset">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li class="active"><span>Сообщении</span></li>
	</ol>
	<div id="offers-container" class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1" style="background-color:white">
		<div class="page-header">
			<h3>Предложении</h3>
		</div>

		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default invoice-panel">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Валюта</a>
					</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
					<div id="currency-body" class="panel-body">
						<div id="filter-body" class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
							<span>Операции:</span>
							<select id="operation-range" class="form-control">
								<option value='today'>Сегодня</option>
								<option value='1week'>1 неделя</option>
								<option value='2week'>2 недели</option>
								<option value='1'>1 месяц</option>
								<option value='3'>3 месяца</option>
								<option value='6'>6 месяцев</option>
								<option value='12'>1 год</option>
							</select>
						</div>
						<div id="currency-msg-body" class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
							@include('currency_messages')
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default invoice-panel">
				<div class="panel-heading" role="tab" id="headingTwo">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">Кредиты</a>
					</h4>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
					<div id="credit-body" class="panel-body">
						<div id="filter-body" class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
							<span>Операции:</span>
							<select id="operation-range" class="form-control">
								<option value='today'>Сегодня</option>
								<option value='1week'>1 неделя</option>
								<option value='2week'>2 недели</option>
								<option value='1'>1 месяц</option>
								<option value='3'>3 месяца</option>
								<option value='6'>6 месяцев</option>
								<option value='12'>1 год</option>
							</select>
						</div>
						<div id="credit-msg-body" class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
							@include('credit_messages')
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default invoice-panel">
				<div class="panel-heading" role="tab" id="headingThree">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">Ликвидность</a>
					</h4>
				</div>
				<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
					<div id="liquid-body" class="panel-body">
						<div id="filter-body" class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
							<span>Операции:</span>
							<select id="operation-range" class="form-control">
								<option value='today'>Сегодня</option>
								<option value='1week'>1 неделя</option>
								<option value='2week'>2 недели</option>
								<option value='1'>1 месяц</option>
								<option value='3'>3 месяца</option>
								<option value='6'>6 месяцев</option>
								<option value='12'>1 год</option>
							</select>
						</div>
						<div id="liquid-msg-body" class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
							@include('liquid_messages')
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@section('js')
<script type="text/javascript">
	$(function() {
		$('#operation-range').on('change', function() {

			if($(this).closest('.panel-body').attr('id') == 'currency-body') {
				var url = '/ajax/messages/currency';
				var messageBody = $('#currency-body #currency-msg-body');
			} else if($(this).closest('.panel-body').attr('id') == 'credit-body') {
				var url = '/ajax/messages/credit';
				var messageBody = $('#credit-body #credit-msg-body');
			} else if($(this).closest('.panel-body').attr('id') == 'liquid-body') {
				var url = '/ajax/messages/liquid';
				var messageBody = $('#credit-body #liquid-msg-body');
			}

			$.ajax({
				url: url,
				dataType: 'json',
				data: {'range_value': $(this).val()},
			})
			.done(function(data) {
				messageBody.html(data);
			});
		});
	});
</script>
@stop
@stop