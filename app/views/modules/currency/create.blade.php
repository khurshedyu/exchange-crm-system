<?php
	$currencies = array(
		'' => 'N/A',
		'RUB' => 'Российский Рубль (RUB)',
		'USD' => 'Американский доллар (USD)',
		'EUR' => 'Евро (EUR)',
		'TJS' => 'Cомони (TJS)',
		'GBP' => 'Фунт стерлингов (GBP)'
	);

	if(isset($type))
		$label_sum = 'Куплю';
	else
		$label_sum = 'Продаю';
?>

@extends('layouts.master')
@section('body')
<div class="row row-bottom-offset">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li class="active"><span>Размещение Валюты</span></li>
	</ol>
	<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1" style="background-color:white">
		<div class="page-header">
			<h3>Размещение Валюты</small></h3>
		</div>
		{{ Form::open(array('route' => 'currency.store', 'files' => 'true')) }}
			<div class="row">
				<div class="form-group col-lg-6 col-md-6 col-sm-6 @if($errors->has('sum')) has-error @endif">
					{{ Form::label('sum', $label_sum) }}
					{{ Form::text('sum', Input::old('sum'), array('class' => 'form-control')) }}
					@if ($errors->has('sum'))
						<p class="help-block">{{ $errors->first('sum') }}</p> 
					@endif
				</div>
				<div class="form-group col-lg-6 col-md-6 col-sm-6 @if($errors->has('type_from')) has-error @endif">
					{{ Form::label('type_from', 'Валюта') }}
					{{ Form::select('type_from', array_except($currencies, array('TJS')), Input::old('type_from'), array('class' => 'form-control')) }}
					@if ($errors->has('type_from'))
						<p class="help-block">{{ $errors->first('type_from') }}</p> 
					@endif
				</div>
			</div>
			<div class="row">
				<div class="form-group col-lg-6 col-md-6 col-sm-6 @if($errors->has('type_to')) has-error @endif">
					{{ Form::label('type_to', 'В обмен на') }}
					{{ Form::select('type_to', $currencies, Input::old('type_to'), array('class' => 'form-control')) }}
						@if ($errors->has('type_to'))
							<p class="help-block">{{ $errors->first('type_to') }}</p> 
						@endif
				</div>
				<div class="form-group col-lg-6 col-md-6 col-sm-6 @if($errors->has('exchange_rate')) has-error @endif">
					{{ Form::label('exchange_rate', 'По Курсу') }}
					{{ Form::text('exchange_rate', Input::old('exchange_rate'), array('class' => 'form-control')) }}
						@if ($errors->has('exchange_rate'))
							<p class="help-block">{{ $errors->first('exchange_rate') }}</p> 
						@endif
				</div>
			</div>
			@if(isset($type))
				{{ Form::hidden('buying', true) }}
			@endif
			<div class="form-group clearfix">
				{{ Form::submit('Разместить', array('class' => 'btn btn-primary btn-md pull-right submit-btn')) }}
			</div>
		{{ Form::close() }}
	</div>
</div>
@stop
@section('js')
<script src="{{ asset('js/currency/view/create/script.js') }}"></script>
@stop