@extends('layouts.master')
@section('body')
<div class="row row-bottom-offset">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li class="active"><span>Предложении по валюте</span></li>
	</ol>
	<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 clearfix" style="background-color:white">
		<div class="page-header">
			<h3>Предложении по Валюте</h3>
		</div>
		<div class="table-responsive">
			<table id="currency" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
		</div>
	</div>
</div>
@stop
@section('js')
<script>
	
	var fees = {{ Config::get('app.fees.currency.rate') }};
	var user_id = {{ Auth::id() }};

	$(function() {

		var params = {
			"bProcessing": true,
			"bServerSide": true,
			"bFilter": false,
			"dom": '<"filter_range">frtip',
			"sAjaxSource": "{{ URL::route('ajax.currencies') }}",

			"fnServerParams": function ( aoData ) {
				aoData.push( { "name": "currency_type", "value": $('#range option:selected').data('currency-type') } );
			},

			"columnDefs": [
				{
					"render": function(data, type, full, meta) {
						if(full.type_from == 'USD')
							return '$' + data;
						else if(full.type_from == 'EUR')
							return '&#8364;' + data;
						else if(full.type_from == 'GBP')
							return '&#163;' + data;
						else if(full.type_from == 'RUB')
							return '₽' + data;
						else if(full.type_from == 'TJS')
							return data + ' Сом';
						else
							return data;

					},

					"targets": 1
				},

				{
					"render": function(data, type, full, meta) {
						if(full.user_id != user_id) {
							if(full.type == 'Куплю') {
								return parseFloat(data) - fees;
							} else {
								return parseFloat(data) + fees;
							}
						} else 
							return data;
					},

					"targets": 2
				},

				{
					"render": function(data, type, full, meta) {
						if(full.user_id != user_id)
							return data;
						else
							return '';
					},

					"targets": 6
				}
			],

			"aoColumns": [
				{'mData': 'id', 'title': ''},
				{'mData': 'sum', 'title': 'Сумма'},
				{'mData': 'exchange_rate', 'title': 'Курс'},
				{'mData': 'type', 'title': 'Куплю/Продам'},
				{'mData': 'exchange_type', 'title': 'Запрашиваемая Валюта'},
				{'mData': 'created_at', 'title': 'Дата Создания'},
				{'mData': 'acquire', 'title': ''},
			],

			"order": [[ 5, "desc" ]],

			"oLanguage": {
				"sProcessing": "Подождите...",
				"sSearch": "Поиск:",
				"sLengthMenu": "Показать _MENU_ записей",
				"sInfo": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"sInfoEmpty": "Записи с 0 до 0 из 0 записей",
				"sInfoFiltered": "(отфильтровано из _MAX_ записей)",
				"sInfoPostFix": "",
				"sLoadingRecords": "Загрузка записей...",
				"sZeroRecords": "Записи отсутствуют.",
				"sEmptyTable:": "В таблице отсутствуют данные",

				"oPaginate": {
					"sFirst": "Первая",
					"sPrevious": "Предыдущая",
					"sNext": "Следующая",
					"sLast": "Последняя"
				},
				
				"oAria": {
					"sSortAscending": ": активировать для сортировки столбца по возрастанию",
					"sSortDescending": ": активировать для сортировки столбца по убыванию"
				}
			}
		}

		var creditTable = $("#currency").dataTable(params);

		$('div.filter_range').html('<select id="range" class="form-control"></select>');
		$('#range').append('<option data-currency-type="">N/A</option>')
			.append('<option data-currency-type="USD">USD</option>')
			.append('<option data-currency-type="TJS">TJS</option>')
			.append('<option data-currency-type="RUB">RUB</option>')
			.append('<option data-currency-type="EUR">EUR</option>')
			.append('<option data-currency-type="GBP">GBP</option>');

		$('#range').on('change', function() {
			creditTable.fnDraw();
		});
		creditTable.on( 'init.dt', function () {
			$('.more-info-popover').popover({html: true});
		});

		creditTable.on('draw.dt', function ( e, settings, json ) {
			$('.more-info-popover').popover();
		});
	});
</script>
@stop