<?php 
$pledge_types = array(
	'' => 'N/A',
	'Портфель' => 'Портфель',
	'Твердый залог' => 'Твердый залог',
	'Ценные Бумаги' => 'Ценные Бумаги',
	'Другое' => 'Другое',
);
?>

{{ Form::open(array('route' => 'newrequest.store', 'id' => 'newrequest_form')) }}
	<div class="form-group">
		{{ Form::label('sum', 'Предлогаемая Цена', array('class' => 'control-label')) }}
		{{ Form::text('sum', null, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('interest_rate', 'Годовая Процентная Ставка', array('class' => 'control-label')) }}
		{{ Form::text('interest_rate', null, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('duration', 'Длительность Погашения Кредита') }}
		{{ Form::text('duration', null, array('id' => 'duration_range')) }}
	</div>

	<div class="form-group">
		{{ Form::label('pledge_type', 'Тип Залога', array('class' => 'control-label')) }}
		{{ Form::select('pledge_type', $pledge_types, null, array('class' => 'form-control')) }}
	</div>
	{{ Form::hidden('liquid_id',null, array('id' => 'liquid_id')) }}
	<div class="form-group clearfix">
		{{ Form::submit('Разместить', array('class' => 'btn btn-primary btn-md pull-right submit-btn')) }}
	</div>
	<div class="clearfix"></div>
{{ Form::close() }}

<script type="text/javascript">
	$(function() {
		$('#duration_range').ionRangeSlider({
			min: 1,
			grid: true,
			postfix: " дней",
			hide_min_max: true,
			from: 6,
			onChange: function(data) {
				$('.irs-single').html(monthToDateStr(data.from_value));
			},
			onUpdate: function(data) {
				$('.irs-single').html(monthToDateStr(data.from_value));
			},
			onStart: function(data) {
				$('.irs-single').html(monthToDateStr(data.from_value));
			},
			onFinish: function(data) {
				$('.irs-single').html(monthToDateStr(data.from_value));
			}
		});
	});

	function monthToDateStr(from) {

		var durationVal;

		if(from == 1) {
			durationVal = from + ' день';
		} else if(from > 1 && from < 5) {
			durationVal = from + ' дня';
		} else if(from >= 5) {
			durationVal = from + ' дней';
		}

		return durationVal;
	}
</script>
