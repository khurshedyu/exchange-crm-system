@extends('layouts.master')
@section('head')
<link rel="stylesheet" href="{{ asset('lightbox-2.7.1/css/lightbox.css') }}">
<script src="{{ asset('lightbox-2.7.1/js/lightbox.min.js') }}"></script>
@stop
@section('body')
<div class="row">
	
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li><a href="/auction">Предложении по Залогу</a></li>
		<li class="active"><span>Дополнительная информация</span></li>
	</ol>

	<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1" style="background-color:white">
		<div class="page-header">
			<h3>Дополнительная информация</small></h3>
		</div>
		<h5>Имущество:</h5>
		<p>{{ $pledge->item }}</p>
		<h5>Стартовая цена:</h5>
		<p>${{ $pledge->start_price }}</p>
		@if($pledge->pledge_description != null)
		<h5>Доп. информация:</h5>
		<p>{{ $pledge->item_details }}</p>
		@endif

		@if($pledge_photos->count() > 0)
			<h5>Фотографии Залога:</h5>
			@foreach($pledge_photos as $photo)
				<a href='{{ asset("pledge-images/$photo->filename") }}' class="pledge-photo-link" data-lightbox="pledge-pictures" data-title="{{ $photo->title }}">
					<div class="thumbnail">
						<img src='{{ asset("pledge-images/$photo->filename") }}' class="portrait">
					</div>
				</a>
			@endforeach
		@endif
	</div>
@stop