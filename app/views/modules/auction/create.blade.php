@extends('layouts.master')
@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-formhelpers.min.css') }}">
@show
@section('body')
<div class="row row-bottom-offset">
	
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li class="active"><span>Размещение залога</span></li>
	</ol>

	<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1" style="background-color:white">
		<div class="page-header">
			<h3>Размещение залога</small></h3>
		</div>
		{{ Form::open(array('route' => 'auction.store', 'files' => 'true')) }}
			<div class="row">
				<div class="form-group col-lg-12">
					<div>
					{{ Form::label('pledge_type_label', 'Тип залога') }} 
					</div>
					<label class="radio-inline">
						{{ Form::radio('pledge_type', 'auction', true) }}
						Не фиксированная
					</label>
					<label class="radio-inline">
					{{ Form::radio('pledge_type', 'fixed', false) }}
					Фиксированная цена
					</label>
				</div>
				<div class="form-group @if($errors->has('item')) has-error @endif col-lg-6 col-md-6 col-sm-6">
					{{ Form::label('item', 'Имущество', array('class' => '@if($errors->has("item")) control-label @endif')) }}
					{{ Form::text('item', Input::old('item'), array('class' => 'form-control')) }}
					@if($errors->has('item'))
						<p class='help-block'>{{ $errors->first('item') }}</p>
					@endif
				</div>

				<div class="form-group @if($errors->has('start_price')) has-error @endif col-lg-6 col-md-6 col-sm-6">
					{{ Form::label('start_price', 'Цена', array('class' => '@if($errors->has("start_price")) control-label @endif')) }}
					{{ Form::text('start_price', Input::old('start_price'), array('class' => 'form-control')) }}
					@if($errors->has('start_price'))
						<p class='help-block'>{{ $errors->first('start_price') }}</p>
					@endif
				</div>

				<div id="auction-time-controls-block" class="clearfix">
					<div class="form-group col-lg-6 col-md-6 col-sm-6">
						{{ Form::label('start_date', 'Дата Начала') }}
						<div class="bfh-datepicker" data-min="today" data-format="d/m/y" data-name="start_date"></div>
					</div>

					<div class="form-group col-lg-6 col-md-6 col-sm-6">
						{{ Form::label('start_time', 'Время Начала') }}
						<div class="bfh-timepicker" data-name="start_time"></div>
					</div>
				</div>


				<div class="form-group col-lg-12">
					{{ Form::label('item_details', 'Детали Залога') }}
					{{ Form::textarea('item_details', Input::old('item_details_content'), array('class' => 'form-control input-block-level', 'rows' => '18', 'name' => 'item_details_content')) }}
				</div>
				<div class="form-group col-lg-12">
					{{ Form::label('request-form', 'Фотографии Залога') }}
					{{ Form::file('pledge_photo[]', array('multiple'=>true, 'id' => 'pledge-images')) }}
					<a class="btn btn-sm btn-primary upload-btn">Выбрать фотографии...</a>
				</div>
				<div class="form-group clearfix col-lg-12">
					{{ Form::submit('Разместить', array('class' => 'btn btn-primary btn-md pull-right submit-btn')) }}
				</div>
			</div>
		{{ Form::close() }}
	</div>
</div>
@stop

@section('js')
<script src="{{ asset('js/auction/view/create/bootstrap-formhelpers.js') }}"></script>
{{-- <script src="{{ asset('js/auction/view/create/bootstrap-formhelpers-datepicker.js') }}"></script> --}}
{{-- <script src="{{ asset('js/auction/view/create/bootstrap-formhelpers-timepicker.js') }}"></script> --}}
<script type="text/javascript">
$(function() {
	@if($errors->count())
		$('textarea[name="item_details_content"]').html(<?php Input::old('item_details_content')?>);
	@endif
});

</script>
<script src="{{ asset('js/auction/view/create/script.js') }}"></script>
@stop