@extends('layouts.master')
@section('head')
<script src="{{ asset('countdown-2.0.4/jquery.countdown.min.js') }}"></script>
@stop
@section('body')
	<div class="row row-bottom-offset">
		<ol class="breadcrumb">
			<li><a href="/">Главная</a></li>
			<li class="active"><span>Предложении по Фиксированному Залогу</span></li>
		</ol>
		<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 clearfix" style="background-color:white">
			<div class="page-header">
				<h3>Предложении по Фиксированному Залогу</h3>
			</div>
			<div class="table-responsive">
				<table id="pledges" class="table t	able-striped table-bordered" cellspacing="0" width="100%"></table>
			</div>
		</div>
	</div>
@stop
@section('js')
	<script>
		$(function() {
			var params = {
				"bProcessing": true,
				"bServerSide": true,
				"bFilter": false,
				"dom": '<"filter_range">frtip',
				"sAjaxSource": "{{ URL::route('ajax.fixed.pledges') }}",

				"fnServerParams": function ( aoData ) {
					aoData.push( { "name": "min", "value": $('#range option:selected').attr('min') } );
					aoData.push( { "name": "max", "value": $('#range option:selected').attr('max') } );
				},

				"columnDefs": [
					{
						"render": function(data) {
							return '$' + data;
						},

						"targets": 3
					}
				],

				"aoColumns": [
					{'mData': 'id', 'title': ''},
					{'mData': 'company_name', 'title': 'Компания'},
					{'mData': 'item', 'title': 'Имущество'},
					{'mData': 'start_price', 'title': 'Цена'},
					{'mData': 'more_info'}
				],
				"oLanguage": {
					"sProcessing": "Подождите...",
					"sSearch": "Поиск:",
					"sLengthMenu": "Показать _MENU_ записей",
					"sInfo": "Записи с _START_ до _END_ из _TOTAL_ записей",
					"sInfoEmpty": "Записи с 0 до 0 из 0 записей",
					"sInfoFiltered": "(отфильтровано из _MAX_ записей)",
					"sInfoPostFix": "",
					"sLoadingRecords": "Загрузка записей...",
					"sZeroRecords": "Записи отсутствуют.",
					"sEmptyTable:": "В таблице отсутствуют данные",

					"oPaginate": {
						"sFirst": "Первая",
						"sPrevious": "Предыдущая",
						"sNext": "Следующая",
						"sLast": "Последняя"
					},
					
					"oAria": {
						"sSortAscending": ": активировать для сортировки столбца по возрастанию",
						"sSortDescending": ": активировать для сортировки столбца по убыванию"
					}
				}
			}

			var creditTable = $("#pledges").dataTable(params);
			
			$('div.filter_range').html('<select id="range" class="form-control"></select>');
			$('#range').append('<option min="" max="">N/A</option>')
				.append('<option min="1" max="1000">до $1000</option>')
				.append('<option min="1001" max="5000">от $1000 до $5000</option>')
				.append('<option min="5001" max="10000">от $5000 до $10000</option>')
				.append('<option min="10001" max="">более $10000</option>');

			$('#range').on('change', function() {
				creditTable.fnDraw();
			});
		});
	</script>
@stop