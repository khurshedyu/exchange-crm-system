@extends('layouts.master')
@section('head')
	<link rel="stylesheet" href="{{ asset('bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}">
@stop
@section('body')
	<div class="row row-bottom-offset">
		<ol class="breadcrumb">
			<li><a href="/">Главная</a></li>
			<li class="active"><span>Предложении по залогу</span></li>
		</ol>
		<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 clearfix" style="background-color:white">
			<div class="page-header">
				<h3>Предложении по Залогу</h3>
			</div>
			<div class="row pledges-row">
				@if($pledges->count()>0)
					@foreach ($pledges as $pledge)
						<div class="col-lg-3 col-md-4 col-sm-6 item-block" data-item-id="{{ $pledge->id }}">
							<h5 class="auction-item-header data-row">{{ $pledge->item }}</h5>
							<div class="clearfix price data-row">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<h4 class="initial-price">
										@if($pledge->max_bet_price == null)
											${{ $pledge->start_price }}
										@else
											${{ $pledge->max_bet_price }}
										@endif
									</h4>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									@if($pledge->max_bet_price == null)
										<input type="text" data-initval="{{ $pledge->start_price }}" class="form-control stepper-input" readonly />
									@else
										<input type="text" data-initval="{{ $pledge->max_bet_price }}" class="form-control stepper-input" readonly />
									@endif
								</div>
							</div>
							<h6 class="company data-row">{{ $pledge->company_name }}</h6>
							<p class="empty data-row"></p>
							<h6 class="lot data-row">Лот №{{ $pledge->id }}</h6>
							<h6 class="completion-time data-row countdown" data-date-countdown="{{$pledge->completion_at}}"></h6>
							<div class="request-block data-row"><button type="button" class="btn btn-primary btn-block request-btn">Предложить</button><a href="auction/details/{{$pledge->id}}" class="btn btn-primary btn-block">Подробнее</a></div>
						</div>
					@endforeach
					{{-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">{{ $pledges->links() }}</div> --}}
				@else
					<div class="col-lg-12"><h5>Активных залогов: 0</h5></div>
				@endif
			</div>
		</div>
	</div>
@stop
@section('js')
	<script type="text/javascript">
		var user_id = {{ Auth::id() }};
	</script>
	<script src="{{ asset('bootstrap-touchspin/jquery.bootstrap-touchspin.js') }}"></script>
	<script src="{{ asset('countdown-2.0.4/jquery.countdown.min.js') }}"></script>
	<script src="{{ asset('js/brain-socket.min.js') }}"></script>
	<script src="{{ asset('js/auction/view/index/script.js') }}"></script>
@stop