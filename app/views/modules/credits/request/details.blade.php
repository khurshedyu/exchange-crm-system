@extends('layouts.master')
@section('head')
<link rel="stylesheet" href="{{ asset('lightbox-2.7.1/css/lightbox.css') }}">
<script src="{{ asset('lightbox-2.7.1/js/lightbox.min.js') }}"></script>
@stop
@section('body')
<div class="row">
	
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li><a href="/credit/request">Запросы по кредитам</a></li>
		<li class="active"><span>Детальная информация</span></li>
	</ol>

	<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1" style="background-color:white">
		<div class="page-header">
			<h3>Детальная информация</small></h3>
		</div>
		<h5>Сумма:</h5>
		<p>${{ $request->sum }}</p>
		<h5>Длительность:</h5>
		<p>{{ $request->duration }} мес.</p>
		<h5>Тип залога:</h5>
		<p>{{ $request->pledge_type }}</p>
		@if($request->pledge_type == 'Твердый Залог')
			@if($request->pledge != null)
				<h5>Залог:</h5>
				<p>{{ $request->pledge }}</p>
			@endif
			
			@if($request->pledge_description != null)
				<h5>Доп. информация:</h5>
				<p>{{ $request->pledge_description }}</p>
			@endif

			@if($request_photos->count() > 0)
				<h5>Изображении:</h5>
				@foreach($request_photos as $photo)
					<a href='{{ asset("pledge-images/$photo->filename") }}' class="pledge-photo-link" data-lightbox="pledge-pictures" data-title="{{ $photo->title }}">
						<div class="thumbnail">
							<img src='{{ asset("pledge-images/$photo->filename") }}' class="portrait">
						</div>
					</a>
				@endforeach
			@endif
		@endif
	</div>
@stop