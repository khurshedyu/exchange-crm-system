<?php

$pledge_types = array(
	'' => 'N/A',
	'Портфель' => 'Портфель',
	'Твердый залог' => 'Твердый Залог',
	'Ценные Бумаги' => 'Ценные Бумаги',
	'Другое' => 'Другое',
);

$currencies = array(
	'' => 'N/A',
	'RUB' => 'Российский Рубль (RUB)',
	'USD' => 'Американский доллар (USD)',
	'EUR' => 'Евро (EUR)',
	'TJS' => 'Cомони (TJS)',
	'GBP' => 'Фунт стерлингов (GBP)'
);

if(isset($type))
	$page_header = ' запроса по кредиту';
else
	$page_header = ' кредита';
?>

@extends('layouts.master')
@section('body')
<div class="row row-bottom-offset">
	
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li class="active"><span>Размещение {{$page_header}}</span></li>
	</ol>

	<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1" style="background-color:white">
		<div class="page-header">
			<h3>Размещение {{ $page_header }}</small></h3>
		</div>
		{{ Form::open(array('route' => 'credit.store', 'files' => 'true')) }}
		<div class="row">
			<div class="form-group @if($errors->has('sum')) has-error @endif col-lg-6">
				{{ Form::label('sum', 'Сумма' . $page_header, array('class' => '@if($errors->has("sum")) control-label @endif')) }}
				{{ Form::text('sum', Input::old('sum'), array('class' => 'form-control')) }}
				@if ($errors->has('sum'))
					<p class="help-block">{{ $errors->first('sum') }}</p> 
				@endif
			</div>

			<div class="form-group @if($errors->has('currency_type')) has-error @endif col-lg-6">
				{{ Form::label('currency_type', 'Валюта', array('class' => '@if($errors->has("currency_type")) control-label @endif')) }}
				{{ Form::select('currency_type', $currencies, Input::old('currency_type'), array('class' => 'form-control')) }}
				@if ($errors->has('currency_type'))
					<p class="help-block">{{ $errors->first('currency_type') }}</p> 
				@endif
			</div>
		</div>
		<div class="row">
			<div class="form-group @if($errors->has('interest_rate')) has-error @endif col-lg-6">
				{{ Form::label('interest_rate', 'Годовая процентная ставка', array('class' => '@if($errors->has("interest_rate")) control-label @endif')) }}
				{{ Form::text('interest_rate', Input::old('interest_rate'), array('class' => 'form-control')) }}
				@if ($errors->has('interest_rate'))
					<p class="help-block">{{ $errors->first('interest_rate') }}</p> 
				@endif
			</div>

			<div class="form-group @if($errors->has('pledge_type')) has-error @endif col-lg-6">
				{{ Form::label('pledge_type', 'Тип залога', array('class' => '@if($errors->has("pledge_type")) control-label @endif')) }}
				{{ Form::select('pledge_type', $pledge_types, Input::old('pledge_type'), array('class' => 'form-control')) }}
				@if ($errors->has('pledge_type'))
					<p class="help-block">{{ $errors->first('pledge_type') }}</p> 
				@endif
			</div>
		</div>
			<div class="form-group col-lg-12">
				{{ Form::label('duration', 'Длительность погашения кредита') }}
				{{ Form::text('duration', Input::old('duration'), array('id' => 'duration_range')) }}
			</div>
			@if(isset($type))
				<div id="pledge-controls-block" style="@if($errors->count() == 0) display: none; @endif">
					<div class="form-group col-lg-12 @if($errors->has('pledge')) has-error @endif">
						{{ Form::label('pledge', 'Залог', array('class' => '@if($errors->has("pledge")) control-label @endif')) }}
						{{ Form::text('pledge', Input::old('pledge'), array('class' => 'form-control')) }}
						@if ($errors->has('pledge'))
							<p class="help-block">{{ $errors->first('pledge') }}</p> 
						@endif
					</div>
					<div class="form-group col-lg-12 @if($errors->has('pledge_details_content')) has-error @endif">
						{{ Form::label('pledge_details', 'Описание Залога', array('class' => '@if($errors->has("pledge_details_content")) control-label @endif')) }}
						<textarea class="input-block-level form-control" id="pledge_details" name="pledge_details_content" rows="18">
							{{ Input::old('pledge_details_content') }}
						</textarea>
						@if ($errors->has('pledge_details_content'))
							<p class="help-block">{{ $errors->first('pledge_details_content') }}</p> 
						@endif
					</div>
					<div class="form-group col-lg-12">
						{{ Form::label('request-form', 'Фотографии залога') }}
						{{ Form::file('pledge_photo[]', array('multiple'=>true, 'id' => 'pledge-images')) }}
						<a class="btn btn-sm btn-primary upload-btn">Выбрать фотографии...</a>
					</div>
				</div>
			@endif
			<div class="form-group clearfix">
				{{ Form::submit('Разместить', array('class' => 'btn btn-primary btn-md pull-right submit-btn')) }}
			</div>
			<div class="clearfix"></div>
		{{ Form::close() }}
	</div>
</div>
@stop
@section('js')
	<script type="text/javascript">
$(function() {
	var durationArr = [1, 3, 6, 12, 24, 36, 48, 60];

	$('#duration_range').ionRangeSlider({
		min: 1,
		grid: true,
		postfix: " месяц",
		hide_min_max: true,
		from: 0,
		values: durationArr,
		onChange: function(data) {
			$('.irs-single').html(monthToDateStr(data.from_value));
		},
		onStart: function(data) {
			$('.irs-single').html(monthToDateStr(data.from_value));
		},
		onFinish: function(data) {
			$('.irs-single').html(monthToDateStr(data.from_value));
		}
	});

	$(".upload-btn").click(function () {
		$("#pledge-images").trigger('click');
	});

	if($('#pledge_type :selected').val() == '') {
		$('#pledge-controls-block').hide();
	}

	$('#pledge_type').on('change', function() {
		if($(this).val() != '')
			$('#pledge-controls-block').show();
		else
			$('#pledge-controls-block').hide();
	});

	$('#pledge-images').MultiFile({
		accept: 'gif|jpg|png',
		max:10,
		STRING: {
			remove: 'x',
			denied: 'Вы не можете использовать $ext формат.\nИспользуйте следующие форматы: "*jpg, *png, *gif".',
			file: '$file',
			selected: 'File selected: $file',
			duplicate: 'This file has already been selected:\n$file',
			toomuch: 'Выбранные изображении превышают максимально допустимый размер ($size)',
			toomany: 'Максимально допустимое количество изображений: $max',
			toobig: '$file is too big (max $size)'
		},
	});

	$('#pledge_details').summernote({
		minHeight: 100,
		maxHeight: 500,
		height: 200,
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'hr']],
			['view', ['fullscreen', 'codeview']]
		],
	});

	$('textarea[name="pledge_details_content"]').html($('#pledge_details').code());

	@if($errors->count())
		var durationRange = $('#duration_range').data("ionRangeSlider");
		durationRange.update({
			from: durationArr.indexOf({{Input::old('duration')}})
		});

		$('textarea[name="pledge_details_content"]').html(<?php trim(Input::old('pledge_details_content'))?>);
	@endif
});

function monthToDateStr(from) {
	var valYear = Math.floor(from / 12);
	var valMonth = from % 12;

	if(valMonth < 5 && valMonth > 1)
		valMonthStr = valMonth + ' месяца';
	else if(valMonth == 1)
		valMonthStr = '1 месяц';
	else if(valMonth >=5 && valMonth < 12)
		valMonthStr = valMonth + ' месяцев';
	else
		valMonthStr = '';

	if(valYear == 0)
		durationVal = valMonthStr;
	else if(valYear == 1)
		durationVal = valYear + ' год ' + valMonthStr;
	else if(valYear > 1 && valYear < 5)
		durationVal = valYear + ' годa ' + valMonthStr;
	else if( valYear >= 5)
		durationVal = valYear + ' лет ' + valMonthStr;
	
	return durationVal;
}
	</script>
@stop