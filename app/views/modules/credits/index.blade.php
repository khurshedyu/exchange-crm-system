<?php
	if(isset($type)) {
		$page_header = ' Запросы по кредитам';
	} else {
		$page_header = ' Предложении по кредитам';
	}

?>

@extends('layouts.master')
@section('body')
<div class="row row-bottom-offset">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li class="active"><span>{{$page_header}}</span></li>
	</ol>

	<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 clearfix" style="background-color:white">
		<div class="page-header">
			<h3>{{ $page_header }}</h3>
		</div>
		<div class="table-responsive">
			<table id="credits" class="table table-striped table-bordered" cellspacing="0" width="100%">

			</table>
		</div>
	</div>
	<div class="modal fade" id="new-request-modal" tabindex="-1" role="dialog" aria-labelledby="newRequestModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Предложение</h4>
				</div>
				<div class="modal-body">
					@if(isset($type))
						@include('modules.credits.request-modal-form')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('js')
<script>
	$(function() {

		var params = {
			"bProcessing": true,
			"bServerSide": true,
			"bFilter": false,
			"dom": '<"filter_range">frtip',

			@if(isset($type))
				"sAjaxSource": "{{ URL::route('ajax.credits.requests') }}",
			@else
				"sAjaxSource": "{{ URL::route('ajax.credits') }}",
			@endif

			"fnServerParams": function ( aoData ) {
				aoData.push( { "name": "min", "value": $('#range option:selected').attr('min') } );
				aoData.push( { "name": "max", "value": $('#range option:selected').attr('max') } );
			},

			"columnDefs": [
				{
					"render": function(data, type, full, meta) {
						if(full.currency_type == 'USD')
							return '$' + data;
						else if(full.currency_type == 'EUR')
							return '&#8364;' + data;
						else if(full.currency_type == 'GBP')
							return '&#163;' + data;
						else if(full.currency_type == 'RUB')
							return '₽' + data;
						else if(full.currency_type == 'TJS')
							return data + ' TJS';
						else
							return data;
					},

					"targets": 1
				},


				{
					"render": function(data) {
						if(data != null)
							return data + '%';
						else
							return '';
					},

					"targets": 2
				},

				{
					"render": function(data) {
						if(data == 1)
							return data + ' месяц';
						else if(data < 5 && data > 1)
							return data + ' месяца';
						else
							return data + ' месяцев';
					},

					"targets": 3
				}

			],

			"aoColumns": [
				{'mData': 'id', 'title': ''},
				{'mData': 'sum', 'title': 'Сумма'},
				{'mData': 'interest_rate', 'title': 'Годовая процентная ставка'},
	

				{'mData': 'duration', 'title': 'Длительность'},
				// {'mData': 'pledge_type', 'title': 'Тип Залога'},

				@if(isset($type))
					{'mData': 'pledge', 'title': 'Залог'},
				@endif

				{'mData': 'created_at', 'title': 'Дата Создания'},

				@if(isset($type))
				// {'mData': 'new_request', 'sClass': 'col-lg-1 col-md-1 col-sm-1 col-xs-2', 'bSortable': false}
				@endif
			],


			@if(isset($type))
			"order": [[ 5, "desc" ]],
			@else
			"order": [[ 4, "desc" ]],
			@endif


			"oLanguage": {
				"sProcessing": "Подождите...",
				"sSearch": "Поиск:",
				"sLengthMenu": "Показать _MENU_ записей",
				"sInfo": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"sInfoEmpty": "Записи с 0 до 0 из 0 записей",
				"sInfoFiltered": "(отфильтровано из _MAX_ записей)",
				"sInfoPostFix": "",
				"sLoadingRecords": "Загрузка записей...",
				"sZeroRecords": "Записи отсутствуют.",
				"sEmptyTable:": "В таблице отсутствуют данные",

				"oPaginate": {
					"sFirst": "Первая",
					"sPrevious": "Предыдущая",
					"sNext": "Следующая",
					"sLast": "Последняя"
				},
				
				"oAria": {
					"sSortAscending": ": активировать для сортировки столбца по возрастанию",
					"sSortDescending": ": активировать для сортировки столбца по убыванию"
				}
			}
		}

		var creditTable = $("#credits").dataTable(params);

		$('div.filter_range').html('<select id="range" class="form-control"></select>');
		$('#range').append('<option min="" max="">N/A</option>')
			.append('<option min="1" max="10000">до $10000</option>')
			.append('<option min="10001" max="50000">от $10000 до $50000</option>')
			.append('<option min="50001" max="100000">от $50000 до $100000</option>')
			.append('<option min="100001" max="">более $100000</option>');

		$('#range').on('change', function() {
			creditTable.fnDraw();
		});
		creditTable.on('init.dt', function () {
			$('.more-info-popover').popover({html: true});
		});

		creditTable.on('draw.dt', function ( e, settings, json ) {
			$('.more-info-popover').popover();
		});

		$.validator.addMethod(
			"regex",
			function(value, element, regexp) {
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			},
			"Please check your input."
		);

		$('#new-request-modal').on('show.bs.modal', function(event) {
			var creditId = $(event.relatedTarget).data('credit-id');
			$(this).find('#credit_id').val(creditId);

			newrequest_validator = $('#newrequest_form').validate({
				rules: {
					sum: {
						required: true,
						number: true
					},
					interest_rate: {
						required: true,
						regex: '^[0-9]{1,2}([,.][0-9]{1,2})?$'
					},
					pledge_type: {
						required: true
					}

				},
				messages: {
					sum: {
						required: 'Поле "Предлогаемая Цена" обязательно для заполнения.'
					},
					interest_rate: {
						required: 'Поле "Годовая Процентная Ставка" обязательно для заполнения.',
						regex: 'Ошибочный формат. Пожалуйста введите числовое значение.'

					},
					pledge_type: {
						required: 'Поле "Тип Залога" обязательно для заполнения.'
					}
				},
				highlight: function(element) {
					$(element).closest('.form-group').addClass('has-error');
				},
				unhighlight: function(element) {
					$(element).closest('.form-group').removeClass('has-error');
				},
				errorElement: 'span',
				errorClass: 'help-block',
				errorPlacement: function(error, element) {
					if(element.parent('.input-group').length) {
						error.insertAfter(element.parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});

		$('#new-request-modal').on('hide.bs.modal', function(event) {
			newrequest_validator.resetForm();
			$('#newrequest_form .has-error').removeClass('has-error');
		});
	});
</script>
@stop