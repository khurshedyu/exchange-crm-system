<?php 
$pledge_types = array(
	'' => 'N/A',
	'Портфель' => 'Портфель',
	'Твердый Залог' => 'Твердый Залог',
	'Ценные Бумаги' => 'Ценные Бумаги',
	'Другое' => 'Другое',
);
?>

{{ Form::open(array('route' => 'newrequest.store', 'id' => 'newrequest_form')) }}
	<div class="form-group">
		{{ Form::label('sum', 'Предлогаемая Сумма', array('class' => 'control-label')) }}
		{{ Form::text('sum', null, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('interest_rate', 'Годовая Процентная Ставка', array('class' => 'control-label')) }}
		{{ Form::text('interest_rate', null, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('duration', 'Длительность Погашения Кредита') }}
		{{ Form::text('duration', null, array('id' => 'duration_range')) }}
	</div>

	<div class="form-group">
		{{ Form::label('pledge_type', 'Тип Залога', array('class' => 'control-label')) }}
		{{ Form::select('pledge_type', $pledge_types, null, array('class' => 'form-control')) }}
	</div>
	{{ Form::hidden('credit_id',null, array('id' => 'credit_id')) }}
	<div class="form-group clearfix">
		{{ Form::submit('Разместить', array('class' => 'btn btn-primary btn-md pull-right submit-btn')) }}
	</div>
	<div class="clearfix"></div>
{{ Form::close() }}

<script type="text/javascript">
	$(function() {
		$('#duration_range').ionRangeSlider({
			min: 1,
			max: 60,
			grid: true,
			postfix: " месяц",
			hide_min_max: true,
			from: 0,
			values: [1, 3, 6, 12, 24, 36, 48, 60],
			onChange: function(data) {
				$('.irs-single').html(monthToDateStr(data.from_value));
			},
			onUpdate: function(data) {
				$('.irs-single').html(monthToDateStr(data.from_value));
			},
			onStart: function(data) {
				$('.irs-single').html(monthToDateStr(data.from_value));
			},
			onFinish: function(data) {
				$('.irs-single').html(monthToDateStr(data.from_value));
			}
		});
	});

	function monthToDateStr(from) {
		var valYear = Math.floor(from / 12);
		var valMonth = from % 12;

		if(valMonth < 5 && valMonth > 1)
			valMonthStr = valMonth + ' месяца';
		else if(valMonth == 1)
			valMonthStr = '1 месяц';
		else if(valMonth >=5 && valMonth < 12)
			valMonthStr = valMonth + ' месяцев';
		else
			valMonthStr = '';

		if(valYear == 0)
				durationVal = valMonthStr;
		else if(valYear == 1)
			durationVal = valYear + ' год ' + valMonthStr;
		else if(valYear > 1 && valYear < 5)
			durationVal = valYear + ' годa ' + valMonthStr;
		else if( valYear >= 5)
			durationVal = valYear + ' лет ' + valMonthStr;
		
		return durationVal;
	}
</script>
