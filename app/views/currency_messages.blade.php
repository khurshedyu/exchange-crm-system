@if($currencyOperation->count()>0)
	@foreach ($currencyOperation as $operation)
		@if($operation->status == 1)
			<div class="alert alert-success operation-alert" role="alert">
			@if($operation->from_user_id == Auth::id())
				<strong>{{ $operation->created_at }}: </strong>
				@if($operation->offers_type == 'Продам')
					<strong>Продано: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@else
					<strong>Куплено: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@endif
				<strong>По курсу: </strong> {{ $operation->offer_rate_from }}
			@else
				<strong>{{ $operation->created_at }}: </strong>
				@if($operation->offers_type == 'Продам')
					<strong>Куплено: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@else
					<strong>Продано: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@endif
				<strong>По курсу: </strong> {{ $operation->offer_rate_to }}
			@endif
			</div>
		@else
			<div class="alert alert-danger operation-alert" role="alert">
			@if($operation->from_user_id == Auth::id())
				<strong>{{ $operation->created_at }}: </strong>
				@if($operation->offers_type == 'Продам')
					<strong>Не Продано: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@else
					<strong>Не Куплено: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@endif
				<strong>По курсу: </strong> {{ $operation->offer_rate_from }}
			@else
				<strong>{{ $operation->created_at }}: </strong>
				@if($operation->offers_type == 'Продам')
					<strong>Не Куплено: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@else
					<strong>Не Продано: </strong> {{ OfferController::getCurrencyTypeWithSum($operation->currency_type) . $operation->offer_sum }}
				@endif
				<strong>По курсу: </strong> {{ $operation->offer_rate_to }}
			@endif
			</div>
		@endif
	@endforeach
	{{ Session::reflash() }}
	{{ Session::flash('messages.currency', $currencyOperation) }}
	@if(!isset($is_pdf))
		<a href="/messages/currency/print/pdf">Распечатать</a>
	@endif
@else
	0 сообщений
@endif