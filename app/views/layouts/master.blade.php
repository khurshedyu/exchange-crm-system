<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Exchange</title>
		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<link rel="stylesheet" href="{{ asset('bootstrap-3.3.1/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('bootflat/css/bootflat.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/style.css') }}">
		<link rel="stylesheet" href="{{ asset('css/responsive-style.css') }}">
		<link rel="stylesheet" href="{{ asset('ionrange-slider/css/normalize.css') }}"
		>
		<link rel="stylesheet" href="{{ asset('ionrange-slider/css/ion.rangeSlider.css') }}">
		<link rel="stylesheet" href="{{ asset('ionrange-slider/css/ion.rangeSlider.skinFlat.css') }}">
		<link rel="stylesheet" href="{{ asset('font-awesome-4.2.0/css/font-awesome.min.css')}}">
		<link rel="stylesheet" href="{{ asset('summernote/summernote.css') }}">
		
		<link rel="stylesheet" href="{{ asset('DataTables-1.10.4/media/css/dataTables.bootstrap.css') }}">
		<script src="{{ asset('jquery-2.1.1/jquery.min.js') }}"></script>
		<script src="{{ asset('DataTables-1.10.4/media/js/jquery.dataTables.js') }}"></script>
		<script src="{{ asset('DataTables-1.10.4/media/js/dataTables.bootstrap.js') }}"></script>
		<script src="{{ asset('miltifile-2.0.3/jQuery.MultiFile.min.js') }}"></script>
		@section('head')
		@show
	</head>
	<body style="background-color:#656D78">

		<div class="container-fluid">
			@if(Session::has('success-message'))
				<div class="row">
					<div class="alert alert-success">
						<strong>Ура!!!</strong> {{ Session::get('success-message') }}
					</div>
				</div>
			@endif

			@if(Session::has('warning-message'))
				<div class="row">
					<div class="alert alert-danger">
						<strong></strong> {{ Session::get('warning-message') }}
					</div>
				</div>
			@endif

			@if(Auth::check() == true)
				<div class="row">
					<nav class="navbar navbar-default" role="navigation">
						<div class="container-fluid">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-4">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span><span class="icon-bar"></span>
								</button>
								<a class="navbar-brand" href="/">
									<img alt="brand" src="{{ asset('img/logo.jpg') }}" style="display:inline-block" id="brand-logo"><span id="logo-title">
										<span >Межбанковская Финансовая Платформа</span>
									</span>
								</a>
							</div>
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-4">
								<p class="navbar-text hidden-xs">Приветствуем вас {{ Auth::user()->first_name }}</p>
								<ul class="nav navbar-nav">
									<li><a href="#"><i class="fa fa-user"></i>Личный Кабинет</a></li>
									<li><a href="/user/messages"><i class="fa fa-comments"></i>Сообщении<span class="badge">{{ Operation::getOperations()->where('created_at', '>=', Carbon::today())->count() }}</span></a></li>
									<li><a href="/logout"><i class="fa fa-sign-out"></i>Выйти</a></li>
								</ul>

							</div>
						</div>
					</nav>
				</div>
			@endif
			@section('body')
			@show
		</div>
		<script src="{{ asset('bootstrap-3.3.1/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('bootflat/js/icheck.min.js') }}"></script>
		<script src="{{ asset('bootflat/js/jquery.fs.selecter.min.js') }}"></script>
		<script src="{{ asset('ionrange-slider/js/ion.rangeSlider.min.js') }}"></script>
		<script src="{{ asset('summernote/summernote.min.js') }}"></script>
		<script src="{{ asset('jquery-validation/jquery.validate.min.js') }}"></script>
		<script src="{{ asset('jquery-validation/messages_ru.js') }}"></script>
		@section('js')
		@show
	</body>
</html>