@extends('layouts.master')
@section('body')
	<div id="loginbox" class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
		<div class="panel panel-info">
			<div class="panel-heading"><h5 class="panel-title">Вход</h5></div>
			<div class="panel-body">
				{{ Form::open(array('route' => 'sessions.store')) }}
					@if($errors->has('email'))
						<label style="color:#da4453">{{ $errors->first('email') }}</label>
					@endif
					<div class="input-group form-group @if($errors->has('email')) has-error has-feedback @endif">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						{{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'Эл. почта')) }}
					</div>

					@if($errors->has('password'))
						<label style="color:#da4453">{{ $errors->first('password') }}</label>
					@endif
					<div class="input-group form-group @if($errors->has('password')) has-error has-feedback @endif">
						<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
						{{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Пароль')) }}
					</div>

					<div class="row">	
						<div class="input-group pull-right">
							<button type="submit" class="btn btn-success login-btn" value="signin">Войти</button>
							<div id="register-btn"><a href="/register" class="btn btn-primary signup-btn">Регистрация</a></div>
						</div>
					</div>

					<div class="row" style="margin-top: 10px;">
						<div style="float: right; margin-left: 10px">
							<a href="register/recover/password"><small class="pull-right">Забыли Пароль?</small></a>
						</div>
						<div class="checkbox"  style="margin-top: -5px;">
							<label>
								{{ Form::checkbox('remember', null, false) }}
								<!-- // <input type="checkbox" value=""> -->
								<small>Запомнить</small>
							</label>
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
@stop