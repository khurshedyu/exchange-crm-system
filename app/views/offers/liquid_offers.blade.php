<?php 

	$newLiquidOffers = Offer::where('to_user_id', Auth::id())
		->where('status', 0)
		->whereIn('module', array('liquid.liquid', 'liquid.request'))
		->get();

	$acceptedLiquidOffers = Offer::where('from_user_id', Auth::id())
		->where('status', 1)
		->whereIn('module', array('liquid.liquid', 'liquid.request'))
		->get();
?>

@if($newLiquidOffers->count() > 0)
	@foreach($newLiquidOffers as $liquid_offer)
		<div class="panel panel-default" data-offer-id="{{ $liquid_offer->id }}" data-offer-status="1" data-module-id-from="{{ $liquid_offer->module_id_from }}" data-module-id-to="{{ $liquid_offer->module_id_to }}" data-module="{{ $liquid_offer->module }}">
			<div class="panel-heading">Кредит</div>
			<div class="panel-body">

				<?php $liquid_info = Liquidity::find($liquid_offer->module_id_from); 
				?>

				
				@if($liquid_offer->module == 'liquid.request')
					<p>У нас есть запрос по вашему кредиту на сумму {{ OfferController::getCurrencyTypeWithSum($liquid_offer->currency_type, $liquid_offer->offer_sum) }}</p>

					<p class="offers-additional-info"><label>Длительность:</label> {{ $liquid_info->duration }} мес.</p>
					<p class="offers-additional-info"><label>Процентная Ставка:</label> {{ $liquid_offer->offer_rate_from }}</p>
					<p class="offers-additional-info"><label>Тип Залога:</label> {{ $liquid_info->pledge_type }}</span>
					<p class="offers-additional-info"><label>Залог:</label> {{ $liquid_info->pledge }}</span>
					<p><label>Описание Залога:</label> {{ $liquid_info->pledge_description }}</span>

					<p>Согласны ли вы выдать кредит по данному запросу?</p>

				@elseif($liquid_offer->module == 'liquid.liquid')
					<p>У нас есть кредит по вашему запросу на сумму {{ OfferController::getCurrencyTypeWithSum($liquid_offer->currency_type, $liquid_offer->offer_sum) }}
					
					<p class="offers-additional-info"><label>Длительность:</label> {{ $liquid_info->duration }} мес.</p>
					<p class="offers-additional-info"><label>Процентная Ставка:</label> {{ $liquid_offer->offer_rate_from }}</p>
					<p class="offers-additional-info"><label>Тип Залога:</label> {{ $liquid_info->pledge_type }}</span>
					<p class="offers-additional-info"><label>Залог:</label> {{ $liquid_info->pledge }}</span>
					<p><label>Описание Залога:</label> {{ $liquid_info->pledge_description }}</span>

					<p>Согласны ли вы получить кредит на эту сумму?</p>
				@endif
			</div>

			<div class="panel-footer clearfix">
				<div class="offer-btns">
					<a class="btn btn-success accept">Да</a>
					<a class="btn btn-danger decline">Нет</a>
				</div>
			</div>
		</div>
	@endforeach
@endif

@if($acceptedLiquidOffers->count() > 0)
	@foreach($acceptedLiquidOffers as $liquid_offer)

		<?php 
			$liquid_info_from = Liquidity::find($liquid_offer->module_id_from);
			$liquid_info_to = Liquidity::find($liquid_offer->module_id_to);
		 ?>

		<div class="panel panel-default" data-offer-id="{{ $liquid_offer->id }}" data-offer-status="2" data-module-id-from="{{ $liquid_offer->module_id_from }}" data-module-id-to="{{ $liquid_offer->module_id_to }}" data-module="{{ $liquid_offer->module }}">
			<div class="panel-heading">Кредит</div>
			<div class="panel-body">
				@if($liquid_offer->module == 'liquid.request')
					<p>Мы готовы предоставить кредит на сумму {{ OfferController::getCurrencyTypeWithSum($liquid_offer->currency_type, $liquid_offer->offer_sum) }}</p>
					<div class="col-lg-6 col-md-6">
						<h5>Запрашиваемый кредит</h5>
						<p class="offers-additional-info"><label>Длительность:</label> {{ $liquid_info_from->duration }} мес.</p>
						<p class="offers-additional-info"><label>Процентная Ставка:</label> {{ $liquid_info_from->interest_rate }}</p>
						<p class="offers-additional-info"><label>Тип Залога:</label> {{ $liquid_info_from->pledge_type }}</span>
						<p class="offers-additional-info"><label>Залог:</label> {{ $liquid_info_from->pledge }}</span>
						<p><label>Описание Залога:</label> {{ $liquid_info_from->pledge_description }}</span>
					</div>
					<div class="col-lg-6 col-md-6">
						<h5>Предоставляемый кредит</h5>
						<p class="offers-additional-info"><label>Длительность:</label> {{ $liquid_info_to->duration }} мес.</p>
						<p class="offers-additional-info"><label>Процентная Ставка:</label> {{ $liquid_info_to->interest_rate }}</p>
						<p class="offers-additional-info"><label>Тип Залога:</label> {{ $liquid_info_to->pledge_type }}</span>
					</div>
					<div class="col-lg-12 col-md-12">
						<p>Согласны ли вы получить кредит по данному запросу?</p>
					</div>
				@elseif($liquid_offer->module == 'liquid.liquid')
					<p>Мы готовы получить кредит на сумму {{ OfferController::getCurrencyTypeWithSum($liquid_offer->currency_type, $liquid_offer->offer_sum) }}
					<div class="col-lg-6 col-md-6">
						<h5>Предоставляемый кредит</h5>
						<p class="offers-additional-info"><label>Длительность:</label> {{ $liquid_info_from->duration }} мес.</p>
						<p class="offers-additional-info"><label>Процентная Ставка:</label> {{ $liquid_info_from->interest_rate }}</p>
						<p class="offers-additional-info"><label>Тип Залога:</label> {{ $liquid_info_from->pledge_type }}</span>
					</div>
					<div class="col-lg-6 col-md-6">
						<h5>Запрашиваемый кредит</h5>
						<p class="offers-additional-info"><label>Длительность:</label> {{ $liquid_info_to->duration }} мес.</p>
						<p class="offers-additional-info"><label>Процентная Ставка:</label> {{ $liquid_info_to->interest_rate }}</p>
						<p class="offers-additional-info"><label>Тип Залога:</label> {{ $liquid_info_to->pledge_type }}</span>
						<p class="offers-additional-info"><label>Залог:</label> {{ $liquid_info_to->pledge }}</span>
						<p><label>Описание Залога:</label> {{ $liquid_info_to->pledge_description }}</span>
					</div>
					<div class="col-lg-12 col-md-12">
						<p>Согласны ли вы предоставить кредит на эту сумму?</p>
					</div>
				@endif
			</div>
			<div class="panel-footer clearfix">
				<div class="offer-btns">
					@if($liquid_offer->module == 'liquid.request')
						<a class="btn btn-success accept">Получить</a>
					@elseif($liquid_offer->module == 'liquid.liquid')	
						<a class="btn btn-success accept">Предоставить</a>
					@endif
					<a class="btn btn-danger decline">Нет</a>
				</div>
			</div>
		</div>
	@endforeach
@endif