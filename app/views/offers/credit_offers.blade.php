<?php 

	$newCreditOffers = Offer::where('to_user_id', Auth::id())
		->where('status', 0)
		->whereIn('module', array('credit.credit', 'credit.request'))
		->get();

	$acceptedCreditOffers = Offer::where('from_user_id', Auth::id())
		->where('status', 1)
		->whereIn('module', array('credit.credit', 'credit.request'))
		->get();
?>

@if($newCreditOffers->count() > 0)
	@foreach($newCreditOffers as $credit_offer)
		<div class="panel panel-default" data-offer-id="{{ $credit_offer->id }}" data-offer-status="1" data-module-id-from="{{ $credit_offer->module_id_from }}" data-module-id-to="{{ $credit_offer->module_id_to }}" data-module="{{ $credit_offer->module }}">
			<div class="panel-heading">Кредит</div>
			<div class="panel-body">

				<?php $credit_info = Credit::find($credit_offer->module_id_from); ?>

				
				@if($credit_offer->module == 'credit.request')
					<p>У нас есть запрос по вашему кредиту на сумму {{ OfferController::getCurrencyTypeWithSum($credit_offer->currency_type, $credit_offer->offer_sum) }}</p>

					<p class="offers-additional-info"><label>Длительность:</label> {{ $credit_info->duration }} мес.</p>
					<p class="offers-additional-info"><label>Процентная Ставка:</label> {{ $credit_offer->offer_rate_from }}</p>
					<p class="offers-additional-info"><label>Тип Залога:</label> {{ $credit_info->pledge_type }}</span>
					<p class="offers-additional-info"><label>Залог:</label> {{ $credit_info->pledge }}</span>
					<p><label>Описание Залога:</label> {{ $credit_info->pledge_description }}</span>

					<p>Согласны ли вы выдать кредит по данному запросу?</p>

				@elseif($credit_offer->module == 'credit.credit')
					<p>У нас есть кредит по вашему запросу на сумму {{ OfferController::getCurrencyTypeWithSum($credit_offer->currency_type, $credit_offer->offer_sum) }}
					
					<p class="offers-additional-info"><label>Длительность:</label> {{ $credit_info->duration }} мес.</p>
					<p class="offers-additional-info"><label>Процентная Ставка:</label> {{ $credit_offer->offer_rate_from }}</p>
					<p class="offers-additional-info"><label>Тип Залога:</label> {{ $credit_info->pledge_type }}</span>
					<p class="offers-additional-info"><label>Залог:</label> {{ $credit_info->pledge }}</span>
					<p><label>Описание Залога:</label> {{ $credit_info->pledge_description }}</span>

					<p>Согласны ли вы получить кредит на эту сумму?</p>
				@endif
			</div>

			<div class="panel-footer clearfix">
				<div class="offer-btns">
					<a class="btn btn-success accept">Да</a>
					<a class="btn btn-danger decline">Нет</a>
				</div>
			</div>
		</div>
	@endforeach
@endif

@if($acceptedCreditOffers->count() > 0)
	@foreach($acceptedCreditOffers as $credit_offer)

		<?php 
			$credit_info_from = Credit::find($credit_offer->module_id_from);
			$credit_info_to = Credit::find($credit_offer->module_id_to);
		 ?>

		<div class="panel panel-default" data-offer-id="{{ $credit_offer->id }}" data-offer-status="2" data-module-id-from="{{ $credit_offer->module_id_from }}" data-module-id-to="{{ $credit_offer->module_id_to }}" data-module="{{ $credit_offer->module }}">
			<div class="panel-heading">Кредит</div>
			<div class="panel-body">
				@if($credit_offer->module == 'credit.request')
					<p>Мы готовы предоставить кредит на сумму {{ OfferController::getCurrencyTypeWithSum($credit_offer->currency_type, $credit_offer->offer_sum) }}</p>
					<div class="col-lg-6 col-md-6">
						<h5>Запрашиваемый кредит</h5>
						<p class="offers-additional-info"><label>Длительность:</label> {{ $credit_info_from->duration }} мес.</p>
						<p class="offers-additional-info"><label>Процентная Ставка:</label> {{ $credit_info_from->interest_rate }}</p>
						<p class="offers-additional-info"><label>Тип Залога:</label> {{ $credit_info_from->pledge_type }}</span>
						<p class="offers-additional-info"><label>Залог:</label> {{ $credit_info_from->pledge }}</span>
						<p><label>Описание Залога:</label> {{ $credit_info_from->pledge_description }}</span>
					</div>
					<div class="col-lg-6 col-md-6">
						<h5>Предоставляемый кредит</h5>
						<p class="offers-additional-info"><label>Длительность:</label> {{ $credit_info_to->duration }} мес.</p>
						<p class="offers-additional-info"><label>Процентная Ставка:</label> {{ $credit_info_to->interest_rate }}</p>
						<p class="offers-additional-info"><label>Тип Залога:</label> {{ $credit_info_to->pledge_type }}</span>
					</div>
					<div class="col-lg-12 col-md-12">
						<p>Согласны ли вы получить кредит по данному запросу?</p>
					</div>
				@elseif($credit_offer->module == 'credit.credit')
					<p>Мы готовы получить кредит на сумму {{ OfferController::getCurrencyTypeWithSum($credit_offer->currency_type, $credit_offer->offer_sum) }}
					<div class="col-lg-6 col-md-6">
						<h5>Предоставляемый кредит</h5>
						<p class="offers-additional-info"><label>Длительность:</label> {{ $credit_info_from->duration }} мес.</p>
						<p class="offers-additional-info"><label>Процентная Ставка:</label> {{ $credit_info_from->interest_rate }}</p>
						<p class="offers-additional-info"><label>Тип Залога:</label> {{ $credit_info_from->pledge_type }}</span>
					</div>
					<div class="col-lg-6 col-md-6">
						<h5>Запрашиваемый кредит</h5>
						<p class="offers-additional-info"><label>Длительность:</label> {{ $credit_info_to->duration }} мес.</p>
						<p class="offers-additional-info"><label>Процентная Ставка:</label> {{ $credit_info_to->interest_rate }}</p>
						<p class="offers-additional-info"><label>Тип Залога:</label> {{ $credit_info_to->pledge_type }}</span>
						<p class="offers-additional-info"><label>Залог:</label> {{ $credit_info_to->pledge }}</span>
						<p><label>Описание Залога:</label> {{ $credit_info_to->pledge_description }}</span>
					</div>
					<div class="col-lg-12 col-md-12">
						<p>Согласны ли вы предоставить кредит на эту сумму?</p>
					</div>
				@endif
			</div>
			<div class="panel-footer clearfix">
				<div class="offer-btns">
					@if($credit_offer->module == 'credit.request')
						<a class="btn btn-success accept">Получить</a>
					@elseif($credit_offer->module == 'credit.credit')	
						<a class="btn btn-success accept">Предоставить</a>
					@endif
					<a class="btn btn-danger decline">Нет</a>
				</div>
			</div>
		</div>
	@endforeach
@endif