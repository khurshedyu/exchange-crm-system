<?php 
	$newCurrencyOffers = Offer::where('to_user_id', Auth::id())
		->where('status', 0)
		->where('module', 'currency')
		->get();

	$acceptedCurrencyOffers = Offer::where('from_user_id', Auth::id())
	->where('status', 1)
	->where('module', 'currency')
	->get();
?>

@if($newCurrencyOffers->count() > 0)
	@foreach($newCurrencyOffers as $currency_offer)
		<div class="panel panel-default" data-offer-id="{{ $currency_offer->id }}" data-offer-status="1" data-module-id-from="{{ $currency_offer->module_id_from }}" data-module-id-to="{{ $currency_offer->module_id_to }}">
			<div class="panel-heading">{{ $currency_offer->offers_type  }}</div>
			<div class="panel-body">
				@if($currency_offer->offers_type == 'Куплю')
					<p>Мы готовы купить {{OfferController::getCurrencyTypeWithSum($currency_offer->currency_type, $currency_offer->offer_sum) }} в обмен на {{ OfferController::getCurrencyTypeWithSum($currency_offer->currency_type_to) }} по вашему курсу: {{ $currency_offer->offer_rate_to }}</p>
					<p>Согласны ли вы произвести обмен на эту сумму?</p>
				@elseif($currency_offer->offers_type == 'Продам')
					<p>Мы готовы продать {{OfferController::getCurrencyTypeWithSum($currency_offer->currency_type, $currency_offer->offer_sum) }} по вашему курсу: {{ $currency_offer->offer_rate_to }}</p>
					<p>Согласны ли вы произвести обмен на эту сумму?</p>
				@endif
			</div>
			<div class="panel-footer clearfix">
				<div class="offer-btns">
					<a class="btn btn-success accept">Да</a>
					<a class="btn btn-danger decline">Нет</a>
				</div>
			</div>
		</div>
	@endforeach
@endif

@if($acceptedCurrencyOffers->count() > 0)
	@foreach($acceptedCurrencyOffers as $currency_offer)
		<div class="panel panel-default" data-offer-id="{{ $currency_offer->id }}" data-offer-status="2" data-module-id-from="{{ $currency_offer->module_id_from }}" data-module-id-to="{{ $currency_offer->module_id_to }}" data-module="currency">
			<div class="panel-heading">
				@if($currency_offer->offers_type == 'Куплю')
					<span>Продам</span>
				@else
					<span>Куплю</span>
				@endif
			</div>

			<div class="panel-body">
				<p>Мы готовы приобрести ваши {{OfferController::getCurrencyTypeWithSum($currency_offer->currency_type, $currency_offer->offer_sum) }} по вашему курсу {{ $currency_offer->offer_rate_from }} в обмен на {{ OfferController::getCurrencyTypeWithSum($currency_offer->currency_type_to) }}</p>
			</div>
			<div class="panel-footer clearfix">
				<div class="offer-btns">
					<a class="btn btn-success accept">Продать</a>
					<a class="btn btn-danger decline">Нет</a>
				</div>
			</div>
		</div>
	@endforeach
@endif