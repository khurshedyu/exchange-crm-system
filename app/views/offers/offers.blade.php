@extends('layouts.master')
@section('body')
<div class="row row-bottom-offset">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li class="active"><span>Предложении</span></li>
	</ol>

	<div id="offers-container" class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 clearfix" style="background-color:white">
		<div class="page-header">
			<h3>Предложении</h3>
		</div>
		
@include('offers.currency_offers')
@include('offers.credit_offers')
@include ('offers.liquid_offers')

	</div>
</div>

@section('js')
<script src="{{ asset('js/brain-socket.min.js') }}"></script>
<script type="text/javascript">
	var prevUrl = "{{ URL::to('/') }}";
</script>
<script src="{{ asset('js/offers/offers-operation.js') }}"></script>
@stop
@stop
