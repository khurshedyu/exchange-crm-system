@extends('layouts.master')
@section('body')
<div class="row modules-row">
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<div>
			<div class="section section-credit">
				<img src="{{ asset('img/icons/credit.png')}}">
				<h4>Кредиты</h4>
			</div>
			<ul class="list-group" style="">
				<li class="list-group-item"><a href="/credit" class="welcome-cat-links">Предложения по кредитам</a></li>
				<li class="list-group-item"><a href="credit/request" class="welcome-cat-links">Запросы по кредиту</a></li>
				<li class="list-group-item"><a href="credit/create" class="welcome-cat-links">Разместить кредит</a></li>
				<li class="list-group-item"><a href="credit/create/request" class="welcome-cat-links">Разместить запрос по кредиту</a></li>
			</ul>
		</div>
	</div>

	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<div>
			<div class="section section-credit">
				<img src="{{ asset('img/icons/exchange.png')}}">
				<h4>Валюта</h4>
			</div>
			<ul class="list-group">
				<li class="list-group-item"><a href="currency/create" class="welcome-cat-links">Продам</a></li>
				<li class="list-group-item"><a href="currency/create/buying" class="welcome-cat-links">Куплю</a></li>
				<li class="list-group-item"><a href="/currency" class="welcome-cat-links">Предложении по купле/продаже</a></li>
				<li class="list-group-item"><a href="">&nbsp</a></li>
			</ul>
		</div>
	</div>

	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<div>
			<div class="section section-credit">
				<img src="{{ asset('img/icons/money_time.png')}}">
				<h4>Ликвидность</h4>
			</div>
			<ul class="list-group">
				<li class="list-group-item"><a href="/liquidity" class="welcome-cat-links">Предложении по кредиту</a></li>
				<li class="list-group-item"><a href="/liquidity/request" class="welcome-cat-links">Запросы по кредиту</a></li>
				<li class="list-group-item"><a href="liquidity/create" class="welcome-cat-links">Разместить кредит</a></li>
				<li class="list-group-item"><a href="liquidity/create/request" class="welcome-cat-links">Разместить запрос по кредиту</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="row modules-row">
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<div>
			<div class="section section-credit">
				<img src="{{ asset('img/icons/cash.png')}}">
				<h4>Обналичка</h4>
			</div>
			<ul class="list-group">
				<li class="list-group-item"><a href="#" class="welcome-cat-links">Предложении наличного капитала</a></li>
				<li class="list-group-item"><a href="#" class="welcome-cat-links">Разместить предложение</a></li>
				<li class="list-group-item"><span>&nbsp</span></li>
			</ul>
		</div>
	</div>

	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<div>
			<div class="section section-credit">
				<img src="{{ asset('img/icons/auction.png')}}">
				<h4>Залоговое имущество</h4>
			</div>
			<ul class="list-group">
				<li class="list-group-item"><a href="auction/create" class="welcome-cat-links">Разместить залог</a></li>
				<li class="list-group-item"><a href="auction" class="welcome-cat-links">Просмотреть преложения по залогам</a></li>
				<li class="list-group-item"><a href="pledges" class="welcome-cat-links">Просмотреть преложения по фиксированным залогам</a></li>
			</ul>
		</div>
	</div>

	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<div>
			<div class="section section-credit">
				<img src="{{ asset('img/icons/papers.png')}}">
				<h4>Ценные бумаги</h4>
			</div>
			<ul class="list-group">
				<li class="list-group-item"><span class="welcome-cat-links">Предложении по облигациям</span></li>
				<li class="list-group-item"><span class="welcome-cat-links">Разместить облигации</span></li>
				<li class="list-group-item"><span>&nbsp</span></li>
			</ul>
		</div>
	</div>
</div>
	<!-- <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12" style="background-color: #8CC152; color:#fff; height:255px;">
		<div style="width:283px; margin: 0 auto;">
			<span class="credits-icon glyphicon glyphicon-credit-card"></span>
			<h3>Кредиты</h3>
			<ul class="list-group">
				<li class="list-group-item"><a href="/credit" class="welcome-cat-links">Просмотр предложений по кредитам</a></li>
				<li class="list-group-item"><a href="credit/request" class="welcome-cat-links">Просмотр запросов по кредитам</a></li>
				<li class="list-group-item"><a href="credit/create" class="welcome-cat-links">Разместить кредит</a></li>
				<li class="list-group-item"><a href="credit/create/request" class="welcome-cat-links">Разместить запрос по кредиту</a></li>
			</ul>
		</div>
	</div>

	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12" style="background-color: #5D9CEC; color:#fff; height:255px;">
		<div style="width:283px; margin: 0 auto;">
			<span class="credits-icon glyphicon glyphicon-euro"></span>
			<h3>Валюта</h3>
			<ul class="list-group">
				<li class="list-group-item"><a href="currency/create" class="welcome-cat-links">Продаю</a></li>
				<li class="list-group-item"><a href="currency/create/buying" class="welcome-cat-links">Куплю</a></li>
				<li class="list-group-item"><a href="/currency" class="welcome-cat-links">Просмотр предложений по валюте</a></li>
			</ul>
		</div>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12" style="background-color: #48CFAD; color:#fff; height:255px;">
		<div style="width:283px; margin: 0 auto;">
			<span class="credits-icon glyphicon glyphicon-credit-card"></span>
			<h3>Ликвидность</h3>
			<ul class="list-group">
				<li class="list-group-item"><a href="/liquidity" class="welcome-cat-links">Просмотр предложений по кредитам</a></li>
				<li class="list-group-item"><a href="/liquidity/request" class="welcome-cat-links">Просмотр запросов по кредитам</a></li>
				<li class="list-group-item"><a href="liquidity/create" class="welcome-cat-links">Разместить кредит</a></li>
				<li class="list-group-item"><a href="liquidity/create/request" class="welcome-cat-links">Разместить запрос по кредиту</a></li>
			</ul>
		</div>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12" style="background-color: #AC92EC; color:#fff; height:255px;">
		<div style="width:283px; margin: 0 auto;">
			<span class="credits-icon glyphicon glyphicon-info-sign"></span>
			<h3>Продажа залога</h3>
			<ul class="list-group">
				<li class="list-group-item"><a href="auction/create" class="welcome-cat-links">Разместить залог</a></li>
				<li class="list-group-item"><a href="auction" class="welcome-cat-links">Просмотреть преложения по залогам</a></li>
				<li class="list-group-item"><a href="pledges" class="welcome-cat-links">Просмотреть преложения по фиксированным залогам</a></li>
			</ul>
		</div>
	</div> -->

@stop
@section('js')
	<script type="text/javascript">
		$(function() {
			$('body').css('background-color', '#F5F7FA');
		});
	</script>
@stop