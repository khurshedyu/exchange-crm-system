<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/






Route::get('/', ['as' => 'home', function()
{
	if(Auth::check() == false)
		return Redirect::guest('login');
	else
		return Redirect::to('/');
}]);




Route::resource('sessions', 'LoginController', ['only' => ['create', 'destroy', 'store']]);
Route::resource('register', 'RegistrationController', ['except' => ['create', 'show', 'edit', 'update', 'destroy']]);

Route::get('login', 'LoginController@create');

/*
|Авторизированная группа
*/
Route::group(array('before' => 'auth'), function() {



	Route::get('logout', 'LoginController@destroy');
	Route::get('/', array('uses' => 'LoginController@welcomePage'));

	Route::get('credit/create/{type?}', array('uses' => 'CreditController@create'));
	Route::get('credit/{type?}', 'CreditController@index');
	Route::get('credit/request/details/{id}', 'CreditController@showRequest');
	Route::resource('credit', 'CreditController');
	Route::get('ajax/credits', array('as'=>'ajax.credits', 'uses'=>'CreditController@getCredit'));
	Route::get('ajax/credits/requests', array('as'=>'ajax.credits.requests', 'uses'=>'CreditController@getCreditRequest'));

	
	Route::get('liquidity/create/{type?}', 'LiquidityController@create');
	Route::get('liquidity/{type?}', 'LiquidityController@index');
	Route::get('liquidity/request/details/{id}', 'LiquidityController@showRequest');
	Route::resource('liquidity', 'LiquidityController');
	Route::get('ajax/liquids', array('as'=>'ajax.liquids', 'uses'=>'LiquidityController@getLiquid'));
	Route::get('ajax/liquids/requests', array('as'=>'ajax.liquids.requests', 'uses'=>'LiquidityController@getLiquidRequest'));

	
	Route::get('currency/create/{type?}', 'CurrenciesController@create');
	Route::resource('currency', 'CurrenciesController');
	Route::post('newrequest', array('as' => 'newrequest.store', 'uses' => 'CreditRequestsController@store'));
	Route::get('ajax/currencies', array('as' => 'ajax.currencies', 'uses' => 'CurrenciesController@getCurrencies'));

	Route::get('auction/details/{id}', 'AuctionController@showPledgeDetails');
	Route::get('pledges/details/{id}', 'AuctionController@showFixedPledgeDetails');
	Route::get('pledges', 'AuctionController@indexFixedPledges');
	Route::resource('auction', 'AuctionController');
	Route::get('ajax/auction/pledges', array('as' => 'ajax.auction.pledges', 'uses' => 'AuctionController@getAuctionPledges'));
	Route::get('ajax/fixed/pledges', array('as' => 'ajax.fixed.pledges', 'uses' => 'AuctionController@getFixedPledges'));
	Route::post('ajax/auction/pledge/newrequest', array('as' => 'ajax.auction.pledge.newrequest', 'uses' => 'AuctionController@postNewRequest'));
	Route::post('ajax/auction/pledge/close', array('as' => 'ajax.auction.pledge.close', 'uses' => 'AuctionController@closePledge'));

	Route::get('offers', array('as' => 'offerroute', function() {
		return View::make('offers/offers');
	}));

	Route::post('ajax/offer/delete', array('as' => 'ajax.offer.delete', 'uses' => 'OfferController@deleteOffer'));
	Route::post('ajax/offer/accept', array('as' => 'ajax.offer.accept', 'uses' => 'OfferController@acceptOffer'));


	Route::get('user/messages', array('as' => 'user.message', 'uses' => 'MessageController@showMessages'));
	Route::get('/ajax/messages/currency', array('as' => 'ajax.user.messages.currencies', 'uses' => 'MessageController@getCurrencyMessages'));
	Route::get('/ajax/messages/credit', array('as' => 'ajax.user.messages.credits', 'uses' => 'MessageController@getCreditMessages'));
	Route::get('/ajax/messages/liquid', array('as' => 'ajax.user.messages.liquid', 'uses' => 'MessageController@getLiquidMessages'));
	Route::get('/messages/currency/print/pdf', array('as' => 'messages.currency.print.pdf', 'uses' => 'MessageController@printCurrencyPDF'));
	Route::get('/messages/credit/print/pdf', array('as' => 'messages.credit.print.pdf', 'uses' => 'MessageController@printCreditPDF'));
	Route::get('/messages/liquid/print/pdf', array('as' => 'messages.liquid.print.pdf', 'uses' => 'MessageController@printLiquidPDF'));

});


/*
|Не авторизированная группа
*/
Route::group(array('before' => 'csrf'), function() {

	Route::post('/sessions', array(
		'as' => 'account-sign-in-post',
		'uses' => 'LoginController@store')
	);

	Route::post('/register', array('uses' => 'RegistrationController@store'));

	Route::post('/register/recover/password', array('as' => 'register.restore.email.send', 'uses' => 'RegistrationController@recoverPassSendMail'));

	Route::post('/register/acount/activate/{code}', array (
		'as' => 'register.restore.password',
		'uses' => 'RegistrationController@changeUserPassword'
	));
});

Route::get('/register/recover/password', 'RegistrationController@getRecoverView');

Route::get('/register/acount/activate/{code}', array(
	'as' => 'activate-account',
	'uses' => 'RegistrationController@getActivate'
));

Route::get('register/recover/password/{code}', array(
	'as' => 'recover-password',
	'uses' => 'RegistrationController@getRecoverPassView'
));
