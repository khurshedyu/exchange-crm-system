<?php

class UsersBet extends \Eloquent {
	protected $table = 'users_bet';
	protected $guarded = ['id', 'user_id', 'pledge_id', 'created_at', 'updated_at'];
}