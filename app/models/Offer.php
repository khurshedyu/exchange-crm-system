<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Offer extends \Eloquent {

	use SoftDeletingTrait;
	
	protected $dates = ['deleted_at'];

	protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

	

	public static function deleteOffersAndModuleRecord($offerId, $moduleId, $module) {

		Offer::find($offerId)->forceDelete();

		if($module == 'currency')
			Currency::find($moduleId)->delete();
		elseif($module == 'credit.request' || $module == 'credit.credit')
			Credit::find($moduleId)->delete();
		elseif($module == 'liquid.request' || $module == 'liquid.liquid')
			Liquidity::find($moduleId)->delete();
	}

	public static function successOffer($offer) {

		Operation::create(
			array(
				'module_id_from' => $offer->module_id_from,
				'module_id_to' => $offer->module_id_to,
				'module' => $offer->module,
				'from_user_id' => $offer->from_user_id,
				'to_user_id' => $offer->to_user_id,
				'offers_type' => $offer->offers_type,
				'offer_sum' => $offer->offer_sum,
				'offer_rate_from' => $offer->offer_rate_from,
				'offer_rate_to' => $offer->offer_rate_to,
				'currency_type' => $offer->currency_type,
				'currency_type_to' => $offer->currency_type_to,
				'status' => '1'
		));

		Offer::find($offer->id)->forceDelete();

		if($offer->module == 'currency') {
			Currency::find($offer->module_id_from)->delete();
			Currency::find($offer->module_id_to)->delete();
		} elseif($offer->module == 'credit.request' || $offer->module == 'credit.credit') {
			Credit::find($offer->module_id_from)->delete();
			Credit::find($offer->module_id_to)->delete();
		} elseif($offer->module == 'liquid.request' || $offer->module == 'liquid.credit') {
			Liquidity::find($offer->module_id_from)->delete();
			Liquidity::find($offer->module_id_to)->delete();
		}
	}
}