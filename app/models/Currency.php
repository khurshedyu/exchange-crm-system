<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Currency extends \Eloquent {
	protected $guarded = ['id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];

	use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
}