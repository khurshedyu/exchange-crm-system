<?php

class CreditRequest extends \Eloquent {
	protected $guarded = ['id', 'user_id', 'credit_id', 'liquid_id', 'created_at', 'updated_at'];
}