<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Credit extends \Eloquent {
	protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

	use SoftDeletingTrait;

	protected $dates = ['deleted_at'];

	public function user() {
		return $this->belongsTo('User');
	}
}