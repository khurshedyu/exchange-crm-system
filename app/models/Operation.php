<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Operation extends \Eloquent {

	use SoftDeletingTrait;
	
	protected $dates = ['deleted_at'];

	protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];


	public static function getOperations() {
		return Operation::where(function ($query) {
				$query->where('from_user_id', Auth::id())
					->orWhere('to_user_id', Auth::id());
			});
	}

}