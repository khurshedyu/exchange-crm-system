<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Liquidity extends \Eloquent {
	use SoftDeletingTrait;

	protected $table = 'liquidity';
	protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $dates = ['deleted_at'];

}