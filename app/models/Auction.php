<?php

class Auction extends \Eloquent {
	protected $table = 'auction';
	protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
}