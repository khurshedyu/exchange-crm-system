<?php 
	Event::listen('toserver.newrequest', function($client_data) {
		$fiveMinutes = Carbon::now()->addMinutes(5);
		Auction::find($client_data->data->item_id)->update(array('completion_at' => $fiveMinutes));

		return BrainSocket::message('toclient.newprice', array('message' => 'Updated', 'new_price' => $client_data->data->new_price, 'item_id' => $client_data->data->item_id, 'user_id' => $client_data->data->user_id, 'newCountdownTime' => $fiveMinutes, 'minPrice' => $client_data->data->new_price + $client_data->data->next_bit));
	});


	Event::listen('currency.offer.newrecord', function($data) {

		if($data->type == 'Куплю') {
			$currency_statement = '<=';
			$offer_type = 'Продам';
		}
		else {
			$currency_statement = '>=';
			$offer_type = 'Куплю';
		}

		$matched_currencies = Currency::where('type', $offer_type)
			->where('user_id', '!=', $data->user_id)
			->where('type_from', $data->type_from)
			->where('type_to', $data->type_to)
			->where('exchange_rate_fees', $currency_statement, $data->exchange_rate)
			->get();

		foreach ($matched_currencies as $record) {
			$offer = new Offer;
			$offer->module_id_from = $data->id;
			$offer->module_id_to = $record->id;
			$offer->from_user_id = $data->user_id;
			$offer->to_user_id = $record->user_id;
			$offer->module = 'currency';
			$offer->offers_type = $offer_type;
			$offer->offer_sum = $data->sum;
			$offer->offer_rate_from = $data->exchange_rate;
			$offer->offer_rate_to = $record->exchange_rate;
			$offer->currency_type = $data->type_from;
			$offer->currency_type_to = $data->type_to;
			$offer->status = 0;
			$offer->save();
		}
	});


	Event::listen('credit.offer.newrecord', function($credit_data) {

		if($credit_data->type != 'request') {
			$type = 'request';
			$offerType = 'credit.credit';
		} else {
			$type = 'credit';
			$offerType = 'credit.request';
		}


		$matched_credit = Credit::where('type', $type)
			->where('user_id', '!=', $credit_data->user_id)
			->where('sum', $credit_data->sum)
			->where('currency_type', $credit_data->currency_type)
			->get();

		foreach ($matched_credit as $credit) {
			$offer = new Offer;
			$offer->module_id_from = $credit_data->id;
			$offer->module_id_to = $credit->id;
			$offer->from_user_id = $credit_data->user_id;
			$offer->to_user_id = $credit->user_id;
			$offer->module = $offerType;
			$offer->offers_type = $credit_data->type;
			$offer->offer_sum = $credit_data->sum;
			$offer->offer_rate_from = $credit_data->interest_rate;
			$offer->offer_rate_to = $credit->interest_rate;
			$offer->currency_type = $credit_data->currency_type;
			$offer->currency_type_to = $credit_data->currency_type;
			$offer->status = 0;
			$offer->save();
		}
	});


	Event::listen('liquid.offer.newrecord', function($liquid_data) {

		if($liquid_data->type != 'request') {
			$type = 'request';
			$offerType = 'liquid.liquid';
		} else {
			$type = 'liquid';
			$offerType = 'liquid.request';
		}


		$matched_liquid = Liquidity::where('type', $type)
			->where('user_id', '!=', $liquid_data->user_id)
			->where('sum', $liquid_data->sum)
			->where('currency_type', $liquid_data->currency_type)
			->get();

		foreach ($matched_liquid as $liquid) {
			$offer = new Offer;
			$offer->module_id_from = $liquid_data->id;
			$offer->module_id_to = $liquid->id;
			$offer->from_user_id = $liquid_data->user_id;
			$offer->to_user_id = $liquid->user_id;
			$offer->module = $offerType;
			$offer->offers_type = $liquid_data->type;
			$offer->offer_sum = $liquid_data->sum;
			$offer->offer_rate_from = $liquid_data->interest_rate;
			$offer->offer_rate_to = $liquid->interest_rate;
			$offer->currency_type = $liquid_data->currency_type;
			$offer->currency_type_to = $liquid_data->currency_type;
			$offer->status = 0;
			$offer->save();
		}
	});

?>