<?php

class UserTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('users')->delete();
		User::create(array(
			'first_name'	=> 'Хуршед',
			'last_name'		=> 'Юсуфов',
			'patronymic'	=> 'Каюмович',
			'phone'			=> '919968118',
			'city'			=> 'Душанбе',
			'company_name'	=> 'АВАК',
			'role'			=> 'admin',
			'email'			=> 'khurshedyu@gmail.com',
			'password'		=> Hash::make('1234'),
			'active'		=> '1',
			'code'			=> '',
		));

		User::create(array(
			'first_name'	=> 'Дилшод',
			'last_name'		=> 'Юсуфов',
			'patronymic'	=> 'Каюмович',
			'phone'			=> '918629991',
			'city'			=> 'Душанбе',
			'company_name'	=> 'АВАК',
			'role'			=> 'admin',
			'email'			=> 'dilshodyu@gmail.com',
			'password'		=> Hash::make('12345'),
			'active'		=> '1',
			'code'			=> '',
		));
	}

}

?>