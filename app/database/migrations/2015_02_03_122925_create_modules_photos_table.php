<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModulesPhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('modules_photos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('credit_id')->nullable();
			$table->integer('liquid_id')->nullable();
			$table->integer('auction_id')->nullable();
			$table->string('title');
			$table->string('filename');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('modules_photos');
	}

}
