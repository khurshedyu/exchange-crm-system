<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLiquidityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('liquidity', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('type', 20);
			$table->integer('sum')->nullable();
			$table->decimal('interest_rate')->nullable();
			$table->string('currency_type');
			$table->integer('duration');
			$table->string('pledge_type', 50)->nullable();
			$table->string('pledge', 50)->nullable();
			$table->longText('pledge_description')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('liquidity');
	}

}
