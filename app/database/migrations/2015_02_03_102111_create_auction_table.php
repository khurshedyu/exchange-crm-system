<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAuctionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('auction', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->text('item', 50);
			$table->integer('start_price');
			$table->longText('item_details')->nullable();
			$table->text('status', 7)->nullable();
			$table->dateTime('completion_at')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('auction');
	}			

}
