<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOffersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('module_id_from');
			$table->integer('module_id_to');
			$table->integer('from_user_id');
			$table->integer('to_user_id');
			$table->string('module');
			$table->string('offers_type');
			$table->decimal('offer_sum');
			$table->decimal('offer_rate_from');
			$table->decimal('offer_rate_to');
			$table->string('currency_type');
			$table->string('currency_type_to');
			$table->integer('status');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offers');
	}

}
