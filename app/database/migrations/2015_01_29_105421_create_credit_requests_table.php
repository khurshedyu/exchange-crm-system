<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCreditRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('credit_requests', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('credit_id')->nullable();
			$table->integer('liquid_id')->nullable();
			$table->integer('user_id');
			$table->integer('sum')->nullable();
			$table->decimal('interest_rate')->nullable();
			$table->integer('duration');
			$table->string('pledge_type', 50)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('credit_requests');
	}

}
